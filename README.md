# IEEE ![](https://travis-ci.org/CalPolyIEEE/ieee.png)
This is the website for the Cal Poly IEEE student branch.

## Tools
This website is powered by [flask](https://github.com/mitsuhiko/flask),
[postgresql](http://www.postgresql.org/), and [heroku](http://heroku.com).


## Requirements
 * [Python 2.7](https://www.python.org/download/releases/2.7/)
    * [pip](https://pypi.python.org/pypi/pip)
 * [Heroku toolbelt](https://toolbelt.heroku.com/)
 * [Postgresql](http://www.postgresql.org/)
 * An `.env` file (ask one of the contributors for a valid one)

## Getting Started

Get your [virtual
environment](http://docs.python-guide.org/en/latest/dev/virtualenvs/) up doing
a quick `virtualenv venv/`. Source it (`. venv/bin/activate`) and install all
the necessary python modules via `pip install -r requirements.txt`.

You'll need a [postgresql server up and
running](http://www.postgresql.org/docs/9.1/static/server-start.html). When
creating your db, please name it `ieee` as all of the makefile rules currently
rely on that name.

Since `calpoly/config.py` takes all of its variables from the environment,
you'll need to supply them using the `.env` file in conjunction with the
command `foreman start` in order to utilize the variables set in the `.env`
file.

The environment file must be stored in the root of this git repo for the
application to work.

After doing all the necessary prep above, you should be good to use the
makefile rules supplied:

  * `make run`

  Run the app.

  * `make update_production_db`

  Runs all necessary commands to migrate the live site's database.

  * `make download_production_db`

    Creates a snapshot of the live site's database and downloads it. Please let
    the other contributors know when you are using this rule. If multiple
    people attempt to grab a snapshot, things can get a little hairy and slow.

  * `make flush_db`

    Removes all tables from the local `ieee` database, runs migrations, and
    then uses `setup.sql` to get all needed rows in the db (like the banner we
    currently have set on the live site).

  * `make rebuild_db`

    Flushes the db and attempts to insert any data that was in the db before
    the flush occurred.

  * `make migrate_db`

    Generates a migration using `alembic`.

  * `make update_docs`

    Updates docs using a `sphinx` command.

  * `make dump`

    Dumps only the data of the local db to a file called `local.db`.

  * `make restore`

    Inserts the `local.db` contents back into the local database.

  * `make clean`

    Removes all `.pyc` files. Sometimes, changes in code aren't reflected when
    testing; this can be due to using the old generated `pyc` files rather than
    the ones that should have been regenerated when rewriting code.

    It's recommended to do a `make clean` before you run the app or as you test
    any changes you've made in the codebase.

## Testing

Currently, we have [travis-ci](https://travis-ci.org/CalPolyIEEE/ieee) setup
for our repo. However, unit tests have not been written (yet!) for either the
backend or frontend. The frontend has `QUnit` setup; the tests simply haven't
been written. `QUnit` test results can be accessed via the
[`/qunit/`](http://127.0.0.1:5000/qunit/) route. The backend tests will
eventually be handled using the `nose` python module.

Travis is running the python code through a style checker. If you would like to
avoid having to rewrite code after commiting it to comply with
[PEP8](http://legacy.python.org/dev/peps/pep-0008/), you should definitely look
into the [`flake8` pre-commit
hook](http://flake8.readthedocs.org/en/latest/vcs.html).


### Local Testing

Most of the administrative actions for the website are accessed via consoles in
your profile. In order to grab all of the consoles available, you need to make
a user you registered into a webmaster. Use the sql function below to make the
user a webmaster.


```sql
SELECT webmaster('<user-email>');
```

After that, login, go to the user's profile, and start playing around with
whatever you need.

If you need direct access to any object stored in the database, head on over to
the admin panel via the [`/admin/`](http://127.0.0.1:5000/admin/) route.


## Deploying

You'll need access to the heroku app for this. Contact one of the contributors
if you intend to push any of your branches to the live site (or just ask them
to do it for you).

The actual deployment is done via a simple push to the heroku remote.

```bash
$ git push heroku master # This assumes you have a git remote called 'heroku'.
```

## Documentation
<img src="http://static.calpolyieee.org/img/doctor.jpg" style="height: 100px; borer-radius: 10px;">

The js here is documented with jsdoc. The documentation is available [here](http://static.calpolyieee.org/jsdoc/).

Python documentation is available [here](http://calpolyieee.github.io/ieee/).

## Questions? Comments? Concerns?

Contact one of the maintainers of the site.

If you can't find their contact information anywhere on the web, just use the
power of git to find them.

```bash
$ git shortlog -s -n -e # Provide sorted commit count of contributors with their emails.
```

### Found a bug?

Good work. Please report it via [github
issues](https://github.com/CalPolyIEEE/ieee/issues) or e-mail one of the
contributors.

# License

Copyright (c) 2014 Cal Poly IEEE-SB

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
