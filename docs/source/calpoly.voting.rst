calpoly.voting package
======================

Submodules
----------

calpoly.voting.models module
----------------------------

.. automodule:: calpoly.voting.models
    :members:
    :undoc-members:
    :show-inheritance:

calpoly.voting.utils module
---------------------------

.. automodule:: calpoly.voting.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: calpoly.voting
    :members:
    :undoc-members:
    :show-inheritance:
