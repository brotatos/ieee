.. Cal Poly IEEE Student Branch Website documentation master file, created by
   sphinx-quickstart on Sat Jun 28 19:59:49 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Cal Poly IEEE Student Branch Website's documentation!
================================================================

Contents:

.. toctree::
   :maxdepth: 2



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Structure
=========

Python
------
Follows MVC.

JS
--
Follows MVC.
