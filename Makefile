run: venv .env
	find . -name '*.pyc' -delete
	. venv/bin/activate && foreman run python2 wsgi.py

clean:
	find . -name '*.pyc' -delete
