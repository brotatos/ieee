from calpoly.parts.models import Part, Kit, IC


def get_part_by_id(partid):
    """
    Returns the part with that specific partid.
    """
    if partid > Kit.KIT_START_ID:
        return Kit.query.get(partid - Kit.KIT_START_ID)
    elif partid < IC.IC_START_ID:
        return Part.query.get(partid)
    else:
        return IC.query.get(partid - IC.IC_START_ID)
