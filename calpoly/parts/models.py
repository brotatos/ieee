from calpoly.core import db
from calpoly.models import register_to_admin


@register_to_admin
class Part(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # This is also the name of part.
    category = db.Column(db.String(120), nullable=False, unique=False)
    price = db.Column(db.Numeric(), nullable=False, unique=False)
    properties = db.relationship('PartProperty',
                                 cascade='all, delete, delete-orphan')
    ic = db.relationship('IC', backref='Part',
                         cascade='all, delete, delete-orphan')

    def __init__(self, category=None, price=None, id=None):
        self.id = id
        self.category = category
        self.price = price

    def __repr__(self):
        # A string is used to represent self.price to prevent truncation of
        # decimal objects.
        return '<Part %s: $%s>' % (self.category, float(self.price) / 100)

    @property
    def serialize(self):
        if (self.ic):
            return self.ic[0].serialize
        else:
            return {
                'id': self.id,
                'price': int(self.price),
                'category': self.category,
                'main_info': PartProperty.query.filter_by(
                    part_id=self.id).filter_by(primary=True).first().serialize,
                'addtl_info': [i.serialize for i in PartProperty.query
                               .filter_by(part_id=self.id)
                               .filter_by(primary=False)]
            }

    @staticmethod
    def sortable():
        return ['category', 'price']


@register_to_admin
class Unit(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Unit %s>' % self.name

    @staticmethod
    def sortable():
        return ['id', 'name']

    @property
    def serialize(self):
        return self.name


@register_to_admin
class Manufacturer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<Manufacturer %s>' % self.name

    @property
    def serialize(self):
        return self.name

    @staticmethod
    def sortable():
        return ['id', 'name']


@register_to_admin
class ChipType(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=True)

    def __init__(self, name=None):
        self.name = name

    def __repr__(self):
        return '<ChipType %s>' % self.name

    @property
    def serialize(self):
        return self.name

    @staticmethod
    def sortable():
        return ['id', 'name']


# AdditionalInfo is an array of PartProperties.
@register_to_admin
class PartProperty(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = db.Column(db.Float, nullable=True, unique=False)
    primary = db.Column(db.Boolean, nullable=False)
    unit = db.relationship('Unit')
    unit_id = db.Column(db.Integer, db.ForeignKey(Unit.id))
    part_id = db.Column(db.Integer, db.ForeignKey(Part.id))

    def __init__(self, value=None, unit_id=None, part_id=None, primary=False):
        self.value = value
        self.unit_id = unit_id
        self.part_id = part_id
        self.primary = primary

    def __repr__(self):
        return '<PartProperty %s>' % self.value

    @staticmethod
    def sortable():
        return ['id', 'value', 'primary']

    @property
    def serialize(self):
        property_json = {
            'id': self.id,
            'name': self.unit.name
        }
        if self.value:
            property_json.update({'value': self.value})
        return property_json


@register_to_admin
class IC(db.Model):
    __tablename__ = 'ic'
    IC_START_ID = 10000
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False, unique=False)
    mftr = db.relationship('Manufacturer')
    mftr_id = db.Column(db.Integer, db.ForeignKey(Manufacturer.id))
    mftr_part_no = db.Column(db.String, nullable=False, unique=True)
    chip_type = db.relationship('ChipType')
    chip_type_id = db.Column(db.Integer, db.ForeignKey(ChipType.id))
    location = db.Column(db.String(100), nullable=False)
    # For an IC, this string will be placed into one of the name value
    # pairs.
    addtlInfo = db.Column(db.String(500), nullable=False, unique=False)
    price = db.Column(db.Numeric(), nullable=False, unique=False)
    stock = db.Column(db.Integer, nullable=False, unique=False)
    part = db.relationship('Part')
    part_id = db.Column(db.Integer, db.ForeignKey(Part.id))

    @staticmethod
    def sortable():
        return ['id', 'name', 'location', 'price', 'stock']

    def __init__(self,
                 name=None,
                 mftr_id=None,
                 mftr_part_no=None,
                 chip_type_id=None,
                 location=None,
                 addtlInfo=None,
                 price=None,
                 stock=None):
        self.name = name
        self.mftr_id = mftr_id
        self.mftr_part_no = mftr_part_no
        self.chip_type_id = chip_type_id
        self.location = location
        self.addtlInfo = addtlInfo
        self.price = price
        self.stock = stock

    def __repr__(self):
        return '<IC %s>' % self.name

    @property
    def serialize(self):
        """Return object data in easily serializeable format."""
        return {
            'id':          self.id + self.IC_START_ID,
            'name':        self.name,
            'main_info':   {'name': self.mftr_part_no},
            'addtl_info':   [{'name': self.addtlInfo}],
            'category':    'IC',
            'price':       int(self.price),
            'part_no':     self.mftr_part_no,
            'mftr':        self.mftr.name,
            'location':    self.location,
            'chip_type':   self.chip_type.name,
            'stock':       self.stock
        }


# may need a margin; ask ieee officers
@register_to_admin
class Kit(db.Model):
    KIT_START_ID = 20000
    id = db.Column(db.Integer, primary_key=True)
    # potentially add relationship to User.Subject like needed for CPE 329
    name = db.Column(db.String(120), nullable=False, unique=False)
    stock = db.Column(db.Integer, nullable=False, unique=False)
    kit_items = db.relationship("KitItem",
                                cascade='all, delete, delete-orphan')
    price = db.Column(db.Numeric(), nullable=False, unique=False)
    location = db.Column(db.String(100), nullable=False, unique=True)

    def __init__(self,
                 name=None,
                 stock=None,
                 items=None,
                 price=None,
                 location=None,
                 kit_items=None):
        self.name = name
        self.stock = stock
        self.price = price
        self.location = location
        self.kit_items = kit_items

    @property
    def serialize(self):
        return {
            'id': self.id,
            'category': 'KIT',
            'main_info': {'name': self.name},
            'name': self.name,
            'price': int(self.price),
            'location': self.location,
            'stock': self.stock,
            'kit_items':   [i.serialize for i in self.kit_items]
        }

    def __repr__(self):
        return '<Kit %s>' % self.name

    @staticmethod
    def sortable():
        return ['id', 'name', 'price', 'stock']

import calpoly.parts.utils


@register_to_admin
class KitItem(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    kit_id = db.Column(db.Integer, db.ForeignKey('kit.id'))
    quantity = db.Column(db.Integer, nullable=False, unique=False)
    # Represents the corresponding part this item holds.
    # This isn't a foreign key because we're fudging the part ids for ICS
    # and static parts
    # i.e. part_id = 2 would be a generic part, 10002 would be an IC
    part_id = db.Column(db.Integer, nullable=False, unique=False)

    def __init__(self, kit_id=None, part_id=None, quantity=None):
        self.kit_id = kit_id
        self.part_id = part_id
        self.quantity = quantity

    def __repr__(self):
        return '<KitItem part_id: %s>' % self.part_id

    @property
    def serialize(self):
        json = calpoly.parts.utils.get_part_by_id(self.part_id).serialize

        json.update({'quantity': self.quantity})
        return json

    @staticmethod
    def sortable():
        return ['kit_id', 'quantity', 'part_id']
