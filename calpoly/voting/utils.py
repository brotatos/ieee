from calpoly.users.models import User
from calpoly.voting.models import Position, Vote


def get_results():
    write_ins = []
    results = {}
    totals = {}
    positions = Position.query.all()

    for position in positions:
        candidates = {}
        for candidate in position.candidates:
            candidates[candidate.name] = 0
        results[position.name] = candidates

    for position in positions:
        votes = Vote.query.filter_by(position_id=position.id).all()
        for vote in votes:
            user = User.query.filter_by(id=vote.candidate_id).first()
            if user.name == "WRITE_IN":
                write_ins.append(position.name + " : " + vote.write_in)
            else:
                results[position.name][user.name] += 1

    for position_name in results:
        totals[position_name] = 0

        for candidate in results[position_name]:
            totals[position_name] += results[position_name][candidate]

    return results, write_ins, totals


def verify_ballot(ballot):
    """
    Verifies all information related to ballot json by checking against the db.
    """
    errors = []
    success = True

    for position, choices in ballot.iteritems():
        dbPos = Position.query.filter_by(name=position).first()
        if not dbPos:
            success = False
            errors.append("Position " + position + "does not exist")
        elif len(choices) != dbPos.multiple and len(dbPos.candidates):
            success = False
            errors.append("You must select " + str(dbPos.multiple) +
                          " candidates for the position " + position)
        else:
            for choice in choices:
                dbUser = User.query.filter_by(name=choice).first()

                if "[WRITE IN]" not in choice and not dbUser:
                    success = False
                    errors.append("Candidate" + choice + "does not exist")

    return {"success": success, "errors": errors}
