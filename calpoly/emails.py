from flask import render_template
from flask.ext.mail import Message
from calpoly.core import mail
from calpoly.config import SENDER
from calpoly.decorators import async
from calpoly.api import create_app


app = create_app()


@async
def send_message(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    with app.app_context():
        mail.send(msg)


def send_reset_password_email(username, user_email, url):
    send_message("[IEEE] Forgotten Password",
                 SENDER,
                 [user_email],
                 render_template("emails/reset_password.txt",
                                 user=username,
                                 url=url),
                 render_template("emails/reset_password.html",
                                 user=username,
                                 url=url))
