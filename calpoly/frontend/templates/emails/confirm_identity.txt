Hi {{ user }},
Please go to the following link to confirm your email address:
{{ url_for('users.gen_confirm_identity',_external=True) }}{{ token }}
Do not reply to this message.
{% include 'emails/signature.txt' %}
