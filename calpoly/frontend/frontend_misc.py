from sqlalchemy.sql.expression import func
from flask import render_template, redirect, Blueprint, g, abort
from flask.ext.login import current_user
from calpoly.users.decorators import requires_roles, login_required
from calpoly.users.models import User
from calpoly.posts.models import ImageLink
from calpoly.parts.models import Kit
from calpoly.projects.models import Project
from calpoly.core_models.models import Config
from calpoly.events.models import Event
from calpoly.posts.models import Post
from calpoly.sponsors.models import Sponsor

mod = Blueprint('frontend_misc', __name__, url_prefix='')


@mod.before_request
def before_request():
    g.user = current_user


@mod.context_processor
def inject_project_and_year():
    """
    Project links are needed in the base templates on all views. This processor
    injects a projects variable into the base template that is now
    automatically added to all views.
    """
    current_projects = Project.query.\
        filter(Project.completed == 'f').values(Project.url, Project.title)
    completed_projects = Project.query.\
        filter(Project.completed == 't').values(Project.url, Project.title)
    return dict(
        current_projects=current_projects,
        completed_projects=completed_projects
    )


@mod.route('/')
def index():
    landing_image = ImageLink.query.get(Config.query.first().landing_image_id)
    sponsors = Sponsor.query.order_by(func.random()).limit(3).all()
    front_page = {
        'projects': [i.serialize for i in Project.query
                     .order_by(func.random()).limit(2)],
        'events': [i.content for i in Event.query.all()],
        'sticky': [i.serialize for i in Post.query.filter_by(sticky=True)
                   .order_by(Post.date.desc())],
        'recent': [i.serialize for i in Post.query.filter_by(sticky=False)
                   .order_by(Post.date.desc()).limit(10)]
    }

    return render_template('index.html',
                           landing_image=landing_image.link,
                           sponsors=sponsors,
                           front_page=front_page)


@mod.route('/about/')
def about():
    return render_template('about.html')


@mod.route('/membership/')
def membership():
    return render_template('membership.html')

@mod.route('/studentmemberbenefits/')
def studentmemberbenefits():
    return render_template('studentmemberbenefits.html')

@mod.route('/tutoring/')
def tutoring():
    return render_template('tutoring.html')


@mod.route('/kits/<string:name>')
def single_kit(name):
    kit = Kit.query.filter_by(name=name).first()

    if not kit:
        return abort(404)
    else:
        return render_template('single_kit.html', kit=kit)


@mod.route('/thelab/')
def the_lab():
    return render_template('thelab.html',
                           status=Config.query.first().lounge_open)


@mod.route('/officers/')
def officers():
    # Order officers them alphabetically by their officer title.
    config = Config.query.first()
    officers_image = ImageLink.query.get(config.officers_image_id)
    officers = [user.serialize for user in User.query.
                filter(User.roles.any(name='officer')).
                order_by(User.officer_title).all()]
    old_officers = [user.serialize for user in User.query.
                    filter(User.roles.any(name='old_officer')).
                    order_by(User.officer_title).all()]
    return render_template(
        'officers.html',
        officers=officers,
        old_officers=old_officers,
        officers_image=officers_image.link
    )


@mod.route('/signup/')
def sign_up():
    return redirect('https://docs.google.com/forms/d/' +
                    '1FPXGWak1KhlY8GlM1eIRS4TZm2rN4O_BZUGSNQ_4t2s/viewform')


@mod.route('/ideas/')
def project_proposals():
    return render_template('projects/proposal.html')


@mod.route('/nixie_clock/')
def nixie_clock():
    return redirect('/projects/nixie_clock/')


@mod.route('/run/')
def run():
    return redirect('/current_user/profile#run')


@mod.route('/summer_camp/')
def summer_camp():
    return render_template('summer_camp.html')


@mod.route('/qunit/')
@login_required
@requires_roles('webmaster')
def qunit():
    return render_template('qunit.html')
