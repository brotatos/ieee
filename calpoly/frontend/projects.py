from flask import Blueprint, render_template
from calpoly.users.decorators import login_required, requires_roles
from calpoly.projects.models import Project
from calpoly.users.models import User

mod = Blueprint('projects', __name__, url_prefix='/projects')


@mod.route('/')
def projects():
    completed = [p.serialize for p in
                 Project.query.filter_by(completed=True).all()]
    not_completed = [p.serialize for p in
                     Project.query.filter_by(completed=False).all()]

    return render_template(
        'projects.html',
        completed=completed,
        not_completed=not_completed
    )


@mod.route('/<string:project_url>/')
def get(project_url):
    project = Project.query.filter_by(url=project_url).first_or_404()
    return render_template('projects/project.html', project=project)


def get_posters():
    return [user.serialize for user in User.query.
            filter(User.roles.any(name='poster')).all()]


@mod.route('/<int:id>/edit/', methods=['GET'])
@login_required
@requires_roles('publicity')
def edit(id):
    project = Project.query.get_or_404(id)
    return render_template(
        'projects/edit.html',
        project=project,
        posters=get_posters()
    )


@mod.route('/new/', methods=['GET'])
@login_required
@requires_roles('publicity')
def new():
    return render_template(
        'projects/edit.html',
        posters=get_posters()
    )
