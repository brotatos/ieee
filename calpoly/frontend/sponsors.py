from flask import Blueprint, render_template
from calpoly.sponsors.models import SponsorType, Sponsor, SponsorProfile
from calpoly.users.models import User


mod = Blueprint('sponsors', __name__, url_prefix='/sponsors')


@mod.route('/')
def index():
    treasurer = User.query.filter(User.officer_title == 'Treasurer').all()
    for user in treasurer:
        if 'old_officer' not in user.get_roles():
            treasurer = user
            break
    sponsors = Sponsor.query.all()
    sponsor_types = SponsorType.query.order_by(SponsorType.min_donation).all()
    return render_template('sponsors/sponsors_index.html',
                           sponsor_types=sponsor_types,
                           sponsors=sponsors,
                           treasurer=treasurer
                           )


@mod.route('/<int:id>/', methods=['GET'])
def sponsor_profile(id):
    sponsor = Sponsor.query.get_or_404(id)
    sponsor_profile = SponsorProfile.query.\
        filter(SponsorProfile.sponsor_id == id).first_or_404()
    return render_template('sponsors/sponsor.html',
                           sponsor=sponsor,
                           sponsor_profile=sponsor_profile)
