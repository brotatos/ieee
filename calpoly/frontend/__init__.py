# -*- coding: utf-8 -*-
"""
    calpoly.frontend
    ~~~~~~~~~~~~

    CalPoly IEEE-SB's frontend.
"""

from flask import render_template
from flask.ext.superadmin import Admin
from calpoly import factory
from calpoly.models import admin_objects
from calpoly.frontend.assets import init_webassets
from calpoly.auth import MyAdminView, MyAdminIndexView


def create_app(settings_override=None):
    """Returns the IEEE API application instance."""

    app = factory.create_app(__name__, __path__, settings_override)
    init_webassets(app)
    # This must be tied to the frontend app in order to set the index_view that
    # prevents not webmasters from accessing the admin panel.
    admin = Admin(app, 'Auth', index_view=MyAdminIndexView())

    for cls in admin_objects:
        admin.register(cls, MyAdminView)

    @app.errorhandler(404)
    def page_not_found(e):
        return render_template('404.html'), 404

    @app.errorhandler(403)
    def forbidden(e):
        return render_template('403.html'), 403

    @app.errorhandler(500)
    def internal_server_error(e):
        return render_template('500.html'), 500

    return app
