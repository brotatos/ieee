import base64
import hmac
import hashlib
from flask import Blueprint, render_template
from flask.ext.login import current_user
from calpoly.users.decorators import login_required, requires_roles
from calpoly.posts.models import Post, Tag
from calpoly.config import S3_ACCESS_ID, S3_SECRET_KEY

mod = Blueprint('posts', __name__, url_prefix='/posts')
policy_document = {
    'expiration': '2016-01-01T00:00:00Z',
    'conditions': [
        {'bucket': 'static.calpolyieee.org'},
        ['starts-with', '$key', 'img/'],
        {'acl': 'public-read'},
        {'success_action_redirect': '/'},
        ['starts-with', '$Content-Type', ''],
        ['content-length-range', 0, 4194304]
    ]
}
policy = base64.b64encode(str(policy_document))
signature = base64.b64encode(
    hmac.new(S3_SECRET_KEY, policy, hashlib.sha1).digest()
)


@mod.route('/<int:post_id>/edit/', methods=['GET', 'POST'])
@login_required
@requires_roles('poster')
def edit_post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template(
        'posts/edit.html',
        post=post,
        s3_access_id=S3_ACCESS_ID,
        policy=policy,
        signature=signature,
        user=current_user
    )


@mod.route('/new/', methods=['GET'])
@login_required
@requires_roles('poster')
def new_post():
    return render_template(
        'posts/edit.html',
        post=None,
        s3_access_id=S3_ACCESS_ID,
        policy=policy,
        signature=signature
    )


@mod.route('/<int:post_id>/', methods=['GET'])
def view_post(post_id):
    post = Post.query.get_or_404(post_id)
    return render_template('posts/single_post.html', post=post)


@mod.route('/with_tag/<string:tag>/', methods=['GET'])
def posts_with_tag(tag):
    tag = Tag.query.filter(Tag.name == tag).first()
    return render_template('posts/with_tag.html', tag=tag)
