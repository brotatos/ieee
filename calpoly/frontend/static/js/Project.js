ieee.Project = function (skel, front_page) {  
    this.skel = skel;
    this.front_page = front_page;
    this.$frame = $("<a>");
    this.$title = $("<span>");
    this.$desc = $("<span>");

    this.$frame.append(this.$title);
    if (front_page) {
        this.$frame.append(this.$desc);
    }
};

ieee.Project.prototype.render = function () {
    this.$frame.attr("href", ieee.baseUrl + "/projects/" + this.skel.url);
    this.$frame.attr("class", "project");

    if (this.skel.image) {
        this.$frame.css("background-image", "url('" + this.skel.image + "')");
    } else {
        this.$frame.addClass("noPic");
    }

    this.$title.text(this.skel.title);
    this.$desc.text(this.skel.description);

    return this.$frame;
};
