/**
 * @file Defines the Position object.
 * @author Jacob Hladky
 */

/**
 * @constructor Position
 * @param {Object} skel - The JSON post skeleton. Sets {@link Position#skel}
 */
ieee.Position = function (skel) {
    /**
     * The number of rows (candidates) clicked right now
     *
     * @name Position#numClicked
     * @type number
     * @default 0
     */
    this.numClicked = 0;

    /**
     * JSON skeleton for the position
     *
     * @name Position#skel
     * @type Object
     */
    this.skel = skel;

    /**
     * HTML frame for the position
     *
     * @name Position#$frame
     * @type jQuery
     */
    this.$frame = $("<div class=\"position\">");

    /**
     * Write-in input box
     *
     * @name Position#$writein
     * @type jQuery
     */
    this.$writein = $("<input type='text' class='form-control'>");

    /**
     * Container for candidates.
     *
     * @name Position#$desc
     * @type jQuery
     */
    this.$candidates = $("<div class=\"candidates\">");

    /**
     * ModalAlert containing more information about the specified candidate
     *
     * @name Position#candidateInfo
     * @type Object
     */
    this.modal = new utils.ModalAlert();

    this.$frame.append("<h4>" + this.skel.title + "</h4>" +
                       "Position Description: " + this.skel.description);

    if (this.skel.multiple > 1) {
        utils.info({
            msg: "You must select " + this.skel.multiple +
                " candidates for this position"
        }, this.$frame);
    }

    this.$candidates.html(this.skel.candidates.map(function (cde) {
        var $cde = ieee.Position._genCde(cde.name);
        var $btn = utils.btn("View Info", "default");

        $btn.click(ieee.Position._showCdeInfo.bind(null, cde, this.modal));
        $cde.click(ieee.Position._selectCde.bind($cde, this)).append($btn);

        return $cde;
    }, this));

    this.$candidates.append($("<div class=\"candidate\">").append(
        "Write in: ", this.$writein));

    this.$frame.append(this.$candidates);
};

/**
 * @function getChoices
 * @memberof Position
 * @instance
 * @summary Collect the choices for this position
 * @returns {string[]} Array of the choices made
 */
ieee.Position.prototype.getChoices = function () {
    var choices = [], writein = this.$writein.val();

    $.each(this.$candidates.children(".active"), function (key, choice) {
        choices.push($(choice).children(".name").text());
    });

    if (writein) {
        choices.push("[WRITE IN] " + writein);
        choices.writein = true;
    }

    return choices;
};

/**
 * @function render
 * @memberof Position
 * @instance
 * @summary Turn the Position's JSON skel into HTML. Idempotent.
 * @returns {jQuery} The HTML representation of the Position
 */
ieee.Position.prototype.render = function () { return this.$frame; };

/**
 * @function _genCde
 * @private
 * @memberof Position
 * @instance
 * @summary Generate a candidate row
 * @param {string} text - The text to go in the row.
 * @returns {jQuery} The candidate row
 */
ieee.Position._genCde = function (text) {
    return $("<div class=\"candidate\">")
        .append("<span class=\"name\">" + text + "</span>");
};

/**
 * @function _selectCde
 * @memberof Position
 * @summary Mark a candidate div as selected. If the `multiple` property
 *          is > 1 select the candidate, otherwise delect the previous choice
 *          and then select the candidate. If this same candidate was
 *          already selected, then de-select it.
 * @param position {Object} - The position object. Since this is an event
 *        handler we want this to be associated with the element the event
 *        happened on, but we still need a reference to position.
 */
ieee.Position._selectCde = function (position) {
    if (position.skel.multiple === 1 && !this.hasClass("active") &&
            position.numClicked === 1) {
        position.$candidates.children(".active").removeClass("active");
        this.addClass("active");
    } else if (this.hasClass("active")) {
        position.numClicked--;
        this.removeClass("active");
    } else if (position.numClicked < position.skel.multiple) {
        this.addClass("active");
        position.numClicked++;
    }
};

/**
 * @function _showCdeInfo
 * @memberof Position
 * @summary Show a modal alert with the candidate's class level and their
 *          answers to the election questions. This function should be
 *          bound to a button click.
 * @param {Object} cde - The candidate.
 * @param {Object} candidateModal - The modal alert to append the candidate
 *        info to.
 * @param {Object} event - The button event.
 */
ieee.Position._showCdeInfo = function (cde, cdeModal, event) {
    var str;

    str = "Class Level: " + (cde.year || "") + "<br><br>";
    str += cde.questions.reduce(function (str, q) {
        return str + "<b>" + q.prompt + "</b><br>" + q.response + "<br><br>";
    }, "");

    $("body").append(cdeModal.render({
        title: "Info for candidate " + cde.name,
        body: str,
        cancel: "Close"
    }));

    cdeModal.show();
    event.stopPropagation();
};
