/**
 * @file Defines the Tag object
 * @author Jacob Hladky
 */

/**
 * @constructor Tag
 * @param {Object} skel - The JSON tag skeleton.
 *        Sets {@link Tag#skel}
 * @Param {number} ndx - The index of the tag in an array.
 *        Sets {@link Tag#ndx}
 */
ieee.Tag = function (skel, ndx) {
    var self = this;
    
    /**
     * JSON skeleton of the Tag.
     *
     * @name Tag#skel
     * @type Object
     */
    this.skel = skel;

    /**
     * Index of the tag in an outside array.
     *
     * @name Tag#ndx
     * @type number
     */
    this.ndx = ndx;

    /**
     * The input HTML element for the tag text.
     *
     * @name Tag#$tag
     * @type jQuery
     */
    this.$tag = $("<input class='postEdit form-control pull-left' " +
                  "type='text'>");
    
    if (!ieee.tagPromise) {
        ieee.tagPromise = $.get(ieee.apiUrl + "/tags/", function (data) {
            ieee._tags = data.tags;
        });
    }
    
    ieee.tagPromise.done(function () {
        self.$tag.autocomplete({
            source: ieee._tags.map(function (tag) { return tag.name; })
        });
    });
};

/**
 * @function render
 * @memberof Tag
 * @instance
 * @summary Turn the Tag into HTML. Idempotent.
 * @returns {jQuery} The HTML representation of the Tag
 */
ieee.Tag.prototype.render = function () {
    return $("<div class='col-sm-2'>").append(this.$tag.val(this.skel.name));
};
