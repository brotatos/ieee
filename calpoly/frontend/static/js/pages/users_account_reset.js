/**
 * @file Defines js for the users account reset page
 * @author Jacob Hladky
 * @namespace ieee.pages.users_account_reset
 */
ieee.pages.users_account_reset = {};

/**
 * @function setup
 * @memberof ieee.pages.users_account_reset
 * @summary Setup the users account reset page.
 */
ieee.pages.users_account_reset.setup = function () {
    var self = this;

    this.$message = $("#arMessage");
    this.$form = $("#arForm");
    this.$email = $("#arEmailIn");
    this.$password = $("#arPasswordIn");
    this.$confirm = $("#arConfirmIn");
    this.$submit = $("#arSubmitBtn");

    // The server tells us in advance if the token is valid.
    // If it it not, abort.
    if (!JSON.parse($("#valid").text())) {
        this.$message.text("Invalid or expired token");
        this.$form.addClass("hidden");
        return;
    }

    this.$submit.click(function () {
        $.post(ieee.apiUrl + "/users/reset/", JSON.stringify({
            token: window.location.href.split("/")[5],
            email: self.$email.val(),
            password: self.$password.val(),
            confirm: self.$confirm.val(),
        }), function (data) {
            if (data.success) {
                utils.success({msg: "Successfully reset account information!" + 
                              " Redirecting to login page..."})

                window.setTimeout(function () {
                    window.location.href = ieee.baseUrl + "/users/login/";
                }, 1000);

            } else {
                data.errors.forEach(function (error) {
                    utils.info({msg: error, dismiss: true});
                });
            }
        });

        return false;
    });
};
