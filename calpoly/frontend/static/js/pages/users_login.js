/**
 * @file Defines js for the login page
 * @author Jacob Hladky
 * @namespace ieee.pages.users_login
 */
ieee.pages.users_login = {
    setup: function () {
        this.loginForm = new utils.Form({
            email: utils.Form.gen.input("Email"),
            password: utils.Form.gen.password("Password"),
            remember_me: utils.Form.gen.checkbox("Remember Me?")
        }, {
            title: "Login",
            url: ieee.apiUrl + "/users/login/",
            successAction: function () {
                window.location.href = ieee.baseUrl + "/current_user/profile/";
            }
        });

        this.loginForm.$confirm.text("Login");
        $("#content").prepend(this.loginForm.render());
    }
};
