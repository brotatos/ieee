/**
 * @file Defines js for the edit/new post page
 * @author Jacob Hladky
 * @namespace ieee.pages.posts_edit
 */
ieee.pages.posts_edit = {};

/**
 * @function setup
 * @memberof ieee.pages.posts_edit
 * @summary Setup the edit post or new post page.
 * @param {Object} [post] - The JSON skeleton of a post object. Supplied
 *        by the jinja2 template if we are editing a post.
 * @param {number} [id] - The id of the current user. Supplied by the
 *        jinja2 template if we are editing a post. This id is checked against
 *        the userid property in the post skeleton. If they do not match,
 *        then the user is not allowed to edit the post.
 */
ieee.pages.posts_edit.setup = function (post, accessKeyId, policy, signature) {
    var self = this;

    this.$post = $("#post");
    this.$tags = $(".form-group.tagStuff");
    this.$addTag = this.$tags.find("i.fa-plus-circle");
    this.$removeTag = this.$tags.find("i.fa-minus-circle");
    this.$uploadBtn = $("#s3Upload a.btn");
    this.$fileIn = $("#s3Upload input");
    this.$deleteBtn = utils.btn("Delete", "danger");

    this.$tagsWrapper = $("<div>");
    this.$imagesWrapper = $("<div>");
    this.$contentWrapper = $("<div>");

    this.tags = []; //holds Tag objects

    this.$tagsWrapper.append(this.$tags);
    this.$imagesWrapper.append(utils.Form._wrap("Upload an Image",
                                                $("#s3Upload")[0]));
    this.$contentWrapper.append(
        utils.Form._wrap("Content", document.createElement("div")),
        "<textarea id=\"postContent\"></textarea>"
    );

    this.postForm = new utils.Form({
        title: utils.Form.gen.input("Post Title"),
        tags: utils.Form.gen.custom(
            this.$tagsWrapper,
            this._tagsVal.bind(this),
            function () {}
        ),
        images: utils.Form.gen.custom(
            this.$imagesWrapper,
            this._imagesVal.bind(this),
            function () {}
        ),
        content: utils.Form.gen.custom(
            this.$contentWrapper,
            this._contentVal.bind(this),
            function () {}
        ),
    }, {
        url: ieee.apiUrl + "/posts/",
        successAction: function () {
            utils.info("Redirecting to profile...", self.postForm.$messages);

            window.setTimeout(function () {
                window.location.href = ieee.baseUrl + "/current_user/profile/";
            }, 2000);
        }
    });

    $("#post+hr").after(this.postForm.render());
    if (post) {
        this.postForm.$confirm.after(this.$deleteBtn);
    }
    this.nicEditor = new nicEditor().panelInstance("postContent");
    
    if (post) {
        this.post = new ieee.Post(post);
        this.$post.append(this.post.render());
        this.postForm.toggleEdit(true);
        this.postForm.populate(post);
        this.postForm.url += post.id + "/";
    } else {
        this.post = new ieee.Post({
            images: [],
            tags: [],
            date: Date.now() / 1000
        });
        this.$post.append(this.post.render());
        this.$removeTag.detach();
    }

    this.postForm.skel.title.keyup(function () {
        self.post.skel.title = $(this).val();
        self.post.render();
    });

    this.$addTag.click(function () {
        var newTag = new ieee.Tag({name: ""}, self.tags.length);

        // If there were zero tags before we append the new tag input
        // then add the remove tag button back.
        if (!self.tags.length) {
            self.$tags.find("#tagControls").append(self.$removeTag);
        }

        self.tags.push(newTag);
        self.$tags.find("#tagControls").before(newTag.render());
        self.post.skel.tags.push(newTag.skel);
        newTag.$tag.keyup(self.tagKeyup(newTag)).keyup();
        self.post.render();
    });

    this.$removeTag.click(function () {
        var toRemove = self.tags.splice(-1, 1);

        toRemove[0].$tag.parent().remove();
        self.post.skel.tags.splice(-1, 1);

        if (!self.tags.length) {
            self.$removeTag.detach();
        }

        self.post.render();
    });

    this.$deleteBtn.click(utils.doubleCheck("delete this post?", function () {
        var url = "/posts/" + post.id + "/";
        
        $.delete_(ieee.apiUrl + url, utils.orAlert(function () {
            utils.info(
                "Post Deleted. Redirecting to profile...",
                self.postForm.$messages
            );

            window.setTimeout(function () {
                window.location.href = ieee.baseUrl + "/current_user/profile/";
            }, 2000);
        }));
    }));
    
    this.$uploadBtn.click(function () {
        var file = self.$fileIn[0].files[0], url =
            "http://static.calpolyieee.org.s3.amazonaws.com/";
        var prop, form = new FormData(), json = {
            key: "img/" + file.name,
            acl: "public-read",
            success_action_redirect: "/",
            AWSAccessKeyId: accessKeyId,
            policy: policy,
            signature: signature,
            "Content-Type": file.type,
            file: file
        };

        self.$uploadBtn.addClass("loading");
        
        for (prop in json) {
            if (json.hasOwnProperty(prop)) {
                form.append(prop, json[prop]);
            }
        }

        $.ajax({
            url: url,
            data: form,
            cache: false,
            contentType: false,
            processData: false,
            type: "POST",
            success: function () {
                var href = url + "img/" + file.name,
                    ndx = self.post.skel.images.length;

                // Show the picture link in the post form
                self.$imagesWrapper.append(self.makePI.call(self, href, ndx));

                // Reset the upload button
                self.$uploadBtn.removeClass("loading");
                
                // Reset the file input
                self.$fileIn.wrap("<form>").closest("form").get(0).reset();
                self.$fileIn.unwrap();
            }
        });

    });

    $(".nicEdit-main").keyup(this.editorKeyup()).click(this.editorKeyup());
};

ieee.pages.posts_edit.makePI = function (href, ndx) {
    var $picInput = $(".hidden .picInputSkel").clone(), self = this;

    $picInput.find("label").text("Picture " + (ndx + 1));
    $picInput.find("p").text(href);
    $picInput.find("a.btn-danger").click(function () {
        $(this).parent().remove();
        self.post.skel.images.splice(ndx, 1);
        self.post.render();
    });

    this.post.skel.images[ndx] = href;
    this.post.render();
    
    return $picInput;
};

ieee.pages.posts_edit._tagsVal = function (val) {
    var self = this;

    if (val) {
        this.tags = val.map(function (tag, ndx) {
            var newTag = new ieee.Tag(tag, ndx);

            self.$tags.find("#tagControls").before(newTag.render());
            newTag.$tag.keyup(self.tagKeyup(newTag)).keyup();
            return newTag;
        });        
    } else {
        return this.tags.map(function (tag) {
            return {name: tag.$tag.val().trim().toLowerCase()};
        });
    }
};

ieee.pages.posts_edit._imagesVal = function (val) {
    var self = this;

    if (val) {
        self.$imagesWrapper.append(val.map(function (href, ndx) {
            return this.makePI(href, ndx);
        }, this));
    } else {
        return this.post.skel.images;
    }
};

ieee.pages.posts_edit._contentVal = function (val) {
    if (val) {
        this.nicEditor.instanceById("postContent").setContent(val);
    } else {
        return "<div>" + this.nicEditor.instanceById("postContent")
            .getContent() + "</div>";
    }
};

/**
 * @function editorKeyup
 * @memberof ieee.pages.officers
 * @summary Generate an event listener that sets the content in the post 
 *          skeleton when the content in the nicEditor changes
 */
ieee.pages.posts_edit.editorKeyup = function () {
    var self = this;

    return function () {
        self.post.skel.content = $("<div>").html(
            self.nicEditor.instanceById("postContent").getContent()
        );
        self.post.render();
    };
};

/**
 * @function tagKeyup
 * @memberof ieee.pages.officers
 * @summary Generate an event listener that sets a tag in the post
 *          skeleton when the tag input changes.
 * @param {Object} tag - The JSON tag, which indicates the index of the tag
 *        to set.
 */
ieee.pages.posts_edit.tagKeyup = function (tag) {
    var self = this;

    return function () {
        self.post.skel.tags[tag.ndx].name = $(this).val();
        self.post.render();
    };
};
