/**
 * @file Defines js for the part location page
 * @author Jacob Hladky
 * @namespace ieee.pages.parts_location
 */
ieee.pages.parts_location = {};

/**
 * @function setup
 * @memberof ieee.pages.parts_location
 * @summary Setup the parts_location page.
 */
ieee.pages.parts_location.setup = function (part) {
    var self = this;

    this.$frame = $("#findFrame");
    this.$title = $("#findTitle");
    this.$addtl = $("#findAddtl");
    this.aTables = ["A1", "A2", "A3", "A4", "A5", "A6"];
    this.bTables = ["B1", "B2"];
    this.tables = {};

    this.aTables.forEach(function (loc) {
        self.tables[loc] = self.genTable(loc, 8, 8);
    });

    this.bTables.forEach(function (loc) {
        self.tables[loc] = self.genTable(loc, 4, 4);
    });

    this.$frame.append(
        $("<div>").append(this.tables.B1, this.tables.B2),
        $("<div>").append(this.tables.A1, this.tables.A3, this.tables.A5),
        $("<div>").append(this.tables.A2, this.tables.A4, this.tables.A6)
    );

    if (part.hasOwnProperty("location")) {
        if (self.setLocation.call(self, part.location))  {
            $("#findTitle").append(new ieee.Part(part).toString());
            $("#findHide").removeClass("hidden");
        }
    } else {
        utils.info({msg: "Location not available for this part."});
    }
};

/**
 * @function setLocation
 * @memberof ieee.pages.parts_location
 * @summary Parse the location string and color the corresp rectangle
 *          in the location tables. Alternately just display a string
 *          indicating some alternate location.
 * @param {string} location - The location string to parse.
 * @returns {boolean} Whether the location string was successfully parsed.
 */
ieee.pages.parts_location.setLocation = function (location) {
    var segs = location.split("-"), success = true;

    if (location[0] === "O") {
        this.$addtl.text(segs[1]);
    } else if (this.aTables.concat(this.bTables).indexOf(segs[0]) === -1) {
        utils.danger({msg: "Failed to parse location data!"});
        //source: http://hdwallpapers.mobiskull.com/a-sad-cats-diary-video-\
        //hd-images-wallpapers.php#prettyPhoto[gallery]/2/
        $("#content").append("<img src='http://static.calpolyieee.org/" +
                             "img/sad_cat.jpg' id='sadCat'>");
        success = false;
    } else {
        this.tables[segs[0]]
            .children("table")
            .children("tbody")
            .children(":nth-child(" + segs[1] + ")")
            .children(":nth-child(" + segs[2] + ")")
            .css("background-color", "red");
        this.$addtl.text(segs[3]);
    }

    return success;
};

/**
 * @function genTable
 * @memberof ieee.pages.parts_location
 * @summary Generate a table that looks like the parts display case, with
 *          specified number of rows and columns.
 * @param {string} loc - The name of the table. e.g. "A1"
 * @param {number} rows - Number of rows in the table.
 * @param {number} cols - Number of columns in the table.
 * @returns {jQuery} The HTML table;
 */
ieee.pages.parts_location.genTable = function (loc, rows, cols) {
    var $table = $("<table>"), $body = $("<tbody>"), i, j;

    $table.append($body);
    for (i = 0; i < rows; i++) {
        var $row = $("<tr>");

        $body.append($row);
        for (j = 0; j < cols; j++) {
            $row.append("<td></td>");
        }
    }

    return $("<div class='vis'>").append($table, "<div>" + loc + "</div>");
};
