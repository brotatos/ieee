/**
 * @file Defines js for the profile page
 * @author Jacob Hladky
 * @namespace ieee.pages.current_user_profile
 */
ieee.pages.current_user_profile = {};

/**
 * @function setup
 * @memberof ieee.pages.current_user_profile
 * @summary Setup the /current_user/profile/ page. For each control
 *          (aka console button) attach a listener that will load the
 *          respective console the first time the console is clicked.
 *          Speficically the HTML, js, and CSS associated with the console
 *          will be loaded. Then the `setup` method of the console is called.
 *          Every subsequent time the button is clicked the `show` method is
 *          called, if defined in the console.
 * @param {Object[]} controls - The list of controls the user has access to.
 *        This is passed to the function by the jinja2 user profile template.
 * @param {bool} debug - Tells the js whether the server is in debug mode
 * @param {unverified} - Whether the user is verified or not
 */
ieee.pages.current_user_profile.setup = function (controls, debug, unverified) {
    var self = this;

    if (unverified) {
        return this.confirmIdentity();
    }

    // Set the debug status globally so any console can access it easily
    ieee.debug = debug;

    this.buttons = {};
    this.controls = {};
    this.loaded = {};
    this.$btns = $("ul.nav.nav-pills");
    this.$nothing = $("#nothing");
    this.$logout = $("#logoutLnk");
    this.curr = null;

    utils.$messages = $("#consoleMessages");
    this.$logout.click(function () {
        $.get(ieee.apiUrl + "/users/logout/", utils.orAlert(function () {
            window.location.href = ieee.baseUrl;
        }));
    });

    controls.forEach(function (control) {
        var $console = $("<div class=\"console hidden\">");
        var $btn = $("<li><a href=\"#" + control.filename + "\">" +
                     control.title + "</a></li>");

        self.buttons[control.filename] = $btn;
        self.controls[control.filename] = $console;
        self.$btns.append($btn);
        $("#content").append($console);

        $btn.click(function () {
            $btn.add(self.buttons[self.curr]).toggleClass("active");

            //console hide/show logic
            if (!self.curr || self.curr === control.filename) {
                self.$nothing.toggleClass("hidden");
                self.controls[control.filename].toggleClass("hidden");
                self.curr = self.curr ? null : control.filename;
            } else {
                self.controls[self.curr].addClass("hidden");
                self.curr = control.filename;
                self.controls[self.curr].removeClass("hidden");
            }

            if (!self.loaded[control.filename]) {
                self.loadConsole($console, control.filename);
            } else if (ieee.consoles[control.filename].show) {
                ieee.consoles[control.filename].show();
            }
        });

        if (window.location.hash === $btn.children("a").attr("href")) {
            $btn.click();
        }
    });
};

/**
 * @function confirmIdentity
 * @memberof ieee.pages.current_user_profile
 * @summary Empty the page and display a message prompting
 *          the user to confirm their identity
 */
ieee.pages.current_user_profile.confirmIdentity = function () {
    var url = ieee.baseUrl + "/users/confirm_identity/";

    this.$confirmBtn = utils.btn("Send confirmation email").addClass("btn-lg");

    utils.$messages = $("#messages");
    utils.info("You must confirm your identity to use the profile page.");
    $("#content, #pic").empty();
    $("#content").append(this.$confirmBtn);

    this.$confirmBtn.click(function () {
        utils.info("Sending confirmation email...");
        $.get(url, utils.orAlert(function () {
            utils.success("Confirmation Email sent.");
        }));
    });
};

/**
 * @function loadConsole
 * @memberof ieee.pages.current_user_profile
 * @summary Load a console.
 * @param {jQuery} $console - The div to put the console HTML into.
 * @param {string} file - The name of the console. This will be used to
 *        determine the name of the HTML, CSS, and js files of the console
 *        to load.
 */
ieee.pages.current_user_profile.loadConsole = function ($console, file) {
    $console.html("<img class=\"loading\" src=\"/static/loading.gif\">");
    $.get(ieee.baseUrl + "/current_user/profile/" + file, function (data) {
        $console.empty().append(data);
    });
    this.loaded[file] = true;
};
