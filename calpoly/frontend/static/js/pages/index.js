/**
 * @file The main page of the website
 * @author Jacob Hladky
 * @namespace ieee.pages.index
 */
ieee.pages.index = {};

/**
 * @function setup
 * @memberof ieee.pages.index
 * @summary Setup the index page.
 */
ieee.pages.index.setup = function (frontPage) {
    var self = this;

    this.recent = [];
    this.$recent = $("#posts");
    this.$sticky = $("#sticky");
    this.$events = $("#eventsList");
    this.$projects = $("#projects");
    this.$noMore = $("#noMore");

    if (frontPage.recent.length) {
        frontPage.recent.forEach(function (post) {
            var newPost = new ieee.Post(post);

            self.recent.push(newPost);
            self.$recent.append(newPost.render());
        });
        $(window).scroll(function () {
            self.onScrollBottom.call(self);
        });
    } else {
        this.$noMore.removeClass("hidden");
    }

    this.$projects.append(frontPage.projects.map(function (project) {
        return new ieee.Project(project, true).render();
    }));

    this.$sticky.append(frontPage.sticky.map(function (post) {
        return new ieee.Post(post).render();
    }));

    this.$events.append(frontPage.events.map(function (event) {
        return "<li class=\"h4\">" + event + "</li>";
    }));

    this.recent[0].$expandLnk.click();
    this.recent[1].$expandLnk.click();

    $(window).resize();
};

/**
 * @function onScrollBottom
 * @memberof ieee.pages.index
 * @summary Called on any scroll event in the window. If the user is scrolled
 *          within 100 pixels of the bottom of the page then remove this
 *          listener and load more posts, then attach a new listener if
 *          there are more posts to load.
 */
ieee.pages.index.onScrollBottom = function () {
    var $win = $(window), self = this;

    if ($win.scrollTop() + $win.height() > $(document).height() - 100) {
        var lastId = self.recent[self.recent.length - 1].skel.id;

        $win.unbind("scroll");
        $.get(ieee.apiUrl + "/posts/recent/" + lastId, function (data) {
            if (data.posts.length) {
                data.posts.forEach(function (post) {
                    var newPost = new ieee.Post(post);

                    self.recent.push(newPost);
                    self.$recent.append(newPost.render());
                });
                $win.scroll(function () {
                    self.onScrollBottom.call(self);
                });
            } else {
                self.$noMore.removeClass("hidden");
            }
        });
    }
};
