/**
 * @file Defines js for the projects page
 * @author Jacob Hladky
 * @namespace ieee.pages.projects
 */
ieee.pages.projects = {};

/**
 * @function setup
 * @memberof ieee.pages.projects
 * @summary Setup the projects page.
 */
ieee.pages.projects.setup = function (completed, notCompleted) {
    $("#prjCurr > div:last-child").append(notCompleted.map(function (prj) {
        return (new ieee.Project(prj)).render();
    }));

    $("#prjPast > div:last-child").append(completed.map(function (prj) {
        return (new ieee.Project(prj)).render();
    }));

    if (!completed.length) {
        $("#prjPast").remove();
    }
};
