/**
 * @file Defines js for the parts page
 * @author Jacob Hladky
 * @namespace ieee.pages.parts
 */
ieee.pages.parts = {};

/**
 * @function setup
 * @memberof ieee.pages.parts
 * @summary Setup the parts page.
 */
ieee.pages.parts.setup = function (categories, kits) {
    var self = this;

    this.$frame = $("#parts");
    this.$results = this.$frame.find("tbody");
    this.$seeAll = utils.btn("");
    this.$title = $("#partsTitle");
    this.$content = $("#parts,#allKits");
    this.$allKits = $("#allKits tbody");
    this.$cnt = this.$frame.find("span.badge");
    this.searchForm = new utils.Form({
        category: utils.Form.gen.select("Category"),
        query: utils.Form.gen.input("Query")
    }, {
        url: ieee.url + "/parts/search/",
        noReset: true,
        successAction: function (data) {
            self.$results.empty().append(data.parts.map(self._mapPart));
            self.$cnt.text(data.parts.length);
            self.searchForm.$confirm.text("Search");
        },
        failAction: function () {
            self.searchForm.$confirm.text("Search");
        }
    });

    this.searchForm.skel.category.fill(categories, true);
    this.searchForm.$confirm.text("Search");
    this.$frame.prepend(this.searchForm.render());

    this.searchForm.$confirm.click(function () {
        this.innerHTML = "<img src=\"/static/loading_green.gif\">";
    });

    this.searchForm.$confirm.after(this.$seeAll);

    this.searchForm.skel.category.change(function () {
        if (this.value !== "Choose...") {
            self.$seeAll.text("See All " + this.value + "S");
        } else {
            self.$seeAll.text("");
        }
    });

    this.$title.find("a").click(function () {
        self.$title.children().toggleClass("active");
        self.$content.toggleClass("hidden");
    });

    this.$seeAll.click(this.seeAll.bind(this.$seeAll[0], this));
    this.$allKits.html(kits.map(this._mapKit));

    if (kits.length === 0) {
        this.$allKits.parent().html($("#noKits"));
    }
    
    if (window.location.hash === "#kits") {
        this.$title.find("li:last-child a").click();
    }
};

ieee.pages.parts.seeAll = function (parts) {
    var param = {filter: "category==" + parts.searchForm.skel.category.val()},
        oldHTML = this.innerHTML;

    this.innerHTML = "<img src=\"/static/loading_blue.gif\">";
    $.get(ieee.url + "/parts/", param, utils.orAlert(function (data) {
        parts.$results.empty().append(data.parts.map(parts._mapPart));
        parts.$cnt.text(data.parts.length);
        parts.$seeAll.html(oldHTML);
    }));
};

ieee.pages.parts._mapKit = function (kit)  {
    return utils.toTr(
        kit.name,
        kit.stock,
        utils.toMoney(kit.price) + " / " +
            utils.toMoney(kit.price * 0.8) + " for members",
        "<a href=\"/kits/" + kit.name + "\">More Info</a>"
    );
};

ieee.pages.parts._mapPart = function (part) {
    var tmp = new ieee.Part(part);

    return utils.toTr(
        part.id,
        tmp + "",
        utils.toMoney(part.price),
        part.category === "IC" && part.addtl_info[0].name ||
            "<em>None</em>"
    );
};
