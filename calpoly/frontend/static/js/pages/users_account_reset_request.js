/**
 * @file Defines js for the users account reset request page
 * @author Jacob Hladky
 * @namespace ieee.pages.users_account_reset_request
 */
ieee.pages.users_account_reset_request = {};

/**
 * @function setup
 * @memberof ieee.pages.users_account_reset_request
 * @summary Setup the users account reset request page.
 */
ieee.pages.users_account_reset_request.setup = function () {
    $("#rstSubmitBtn").click(function () {
        $.post(
            ieee.apiUrl + "/users/reset_account/",
            JSON.stringify({email: $("#rstEmailIn").val()}),
            function (data) {
                if (data.success) {
                    utils.success({
                        msg: "Instructions regarding an account " + 
                            "reset have been sent to your email address"
                    });
                } else {
                    data.errors.forEach(function (error) {
                        utils.info({msg: error, dismiss: true});
                    });
                }
            }
        );
        return false;
    });
};
