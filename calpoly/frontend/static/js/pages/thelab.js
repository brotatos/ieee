ieee.pages.thelab = {};

ieee.pages.thelab.setup = function (status) {
    this.$status = $("#labStatus");

    if (status) {
        this.$status.removeClass("closed");
        this.$status.children("span").text("OPEN");
    }
};
