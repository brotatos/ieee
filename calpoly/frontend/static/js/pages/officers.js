/**
 * @file Defines js for the officers page
 * @author Jacob Hladky
 * @namespace ieee.pages.officers
 */
ieee.pages.officers = {};

/**
 * @function setup
 * @memberof ieee.pages.officers
 * @summary Setup the officers page.
 */
ieee.pages.officers.setup = function (officers, old_officers) {
    var self = this;
    var titles = ["Branch President", "Vice President External",
                  "Vice President Internal", "Treasurer", "Secretary"];
    var executive = {}, other = [], selected = "#offExec";

    this.$exec = $("#offExec");
    this.$other = $("#offOther");
    this.$old = $("#offOld");
    this.$choose = $("#offChoose");

    this.$choose.children().click(function () {
        var $this = $(this);

        self.$choose.children("[sec=" + selected + "]").removeClass("selected");
        $(selected).addClass("hidden");
        $this.addClass("selected");
        $($this.attr("sec")).removeClass("hidden");
        selected = $this.attr("sec");

        return false;
    });

    officers.forEach(function (officer) {
        if (titles.indexOf(officer.title) !== -1) {
            executive[officer.title] = officer;
        } else {
            other.push(officer);
        }
    });

    this.$exec.append(titles.map(function (title) {
        return $("<div class='exec'>").append(
                "<div class='h2'>" + title + "</div>",
            new ieee.Profile(executive[title]).render({no_title: true})
        );
    }));
    
    this.$other.append(other.map(function (officer) {
        return $("<div>").append(
            new ieee.Profile(officer).render(),
            "<div class=\"clearfix\">"
        );
    }));

    this.$old.append(old_officers.map(function (officer) {
        return (new ieee.Profile(officer)).render({old: true});
    }));
    
    if (window.location.hash) {
        this.$choose.children("[sec=" + window.location.hash + "]").click();
    }
};
