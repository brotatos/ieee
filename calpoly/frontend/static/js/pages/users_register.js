/**
 * @file Defines js for the register page
 * @author Jacob Hladky
 * @namespace ieee.pages.users_register
 */
ieee.pages.users_register = {};

/**
 * @function setup
 * @memberof ieee.pages.users_register
 * @summary Setup the users register page.
 */
ieee.pages.users_register.setup = function () {
    var self = this, msg = "Must be an @calpoly.edu address";

    this.registerForm = new utils.Form({
        email: utils.Form.gen.input("Email Address", msg),
        password: utils.Form.gen.password("Password"),
        confirm: utils.Form.gen.password("Confirm Password"),
        challenge: utils.Form.gen.p("&nbsp", "<div id=\"regCaptcha\"></div>")
    }, {
        title: "Register",
        url: ieee.apiUrl + "/users/",
        preSubmit: function (json) {
            json.key = "";
            json.challenge = Recaptcha.get_challenge();
            json.response = Recaptcha.get_response();
            self.captcha();
        },
        successAction: function () {
            window.location.href = ieee.baseUrl + "/current_user/profile";
        }
    });

    this.captcha();
    this.registerForm.$confirm.text("Register");
    $("#content").append(this.registerForm.render());
};

/**
 * @function captcha
 * @memberof ieee.pages.users_register
 * @summary Reinitialize the captcha.
 */
ieee.pages.users_register.captcha = function () {
    Recaptcha.create(
        "6Lfla-YSAAAAAJusSwt2ER9ZWaardqWLdfzcQ8-b",
        "regCaptcha",
        {theme: "clean"}
    );
};
