/**
 * @file Project Builder page js.
 * @author Jacob Hladky
 * @namespace ieee.pages.projects_edit
 */
ieee.pages.projects_edit = {};

/**
 * @function setup
 * @memberof ieee.pages.projects_edit
 * @summary Setup the edit project page.
 */
ieee.pages.projects_edit.setup = function (posters, project) {
    var self = this, formConfig;

    this.$frame = $("#project");
    this.$contentWrapper = $("<div>");
    this.$contentWrapper.append("<h5>Content</h5>" +
                                "<textarea id=\"prjContent\"></textarea>");

    this.prjForm = new utils.Form({
        title: utils.Form.gen.input("Project Title"),
        completed: utils.Form.gen.checkbox("Completed"),
        repo: utils.Form.gen.input("Github Repo", "optional"),
        contact_id: utils.Form.gen.select("Project Contact"),
        image: utils.Form.gen.input("Project Image"),
        caption: utils.Form.gen.textarea("Image Caption", 2),
        description: utils.Form.gen.textarea("Description", 4),
        content: utils.Form.gen.custom(
            this.$contentWrapper,
            this._contentVal.bind(this),
            function () {}
        ),
    }, {url: ieee.apiUrl + "/projects/"});
    
    this.$frame.prepend(this.prjForm.render());

    this.prjForm.skel.contact_id.fill(posters.map(function (p) {
        return {val: p.id, text: p.name ||  ""};
    }), true);

    this.prjForm.skel.image.keyup(function () {
        var $img = $("<img class=\"hidden\">");

        $("body").append($img);
        $img.error(function () {
            utils.Form._status(self.prjForm.skel.image[0], "error");
            $img.remove();
        });
        $img.load(function () {
            utils.Form._status(self.prjForm.skel.image[0], "success");
            $img.remove();
        });
        $img.attr("src", $(this).val());
    });

    this.nicEditor = new nicEditor().panelInstance("prjContent");
    
    if (project) {
        this.prjForm.toggleEdit(true);
        this.prjForm.populate(project);
        this.prjForm.url += project.id + "/";
    }
};

/**
 * @function _contentVal
 * @private
 * @memberof ieee.pages.projects_edit
 * @summary Get of set the value of the nicEditor
 * @param {Object} skel - JSON skeleton.
 */
ieee.pages.projects_edit._contentVal = function (val) {
    var self = this;

    if (val) {
        this.nicEditor.instanceById("prjContent").setContent(val);
    } else {
        return "<div>" + this.nicEditor.instanceById("prjContent")
            .getContent() + "</div>"
    }
};
