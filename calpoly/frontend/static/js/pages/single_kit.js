ieee.pages.single_kit = {};

ieee.pages.single_kit.setup = function (kit) {
    var tM = utils.toMoney;

    $(".panel-title > span > span")
        .text(tM(kit.price) + " / " + tM(kit.price * 0.8) + " for members");

    $("#content .panel tbody").html(kit.kit_items.map(function (item) {
        return utils.toTr(item.quantity, new ieee.Part(item).toString());
    }));
};
