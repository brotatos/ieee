/**
 * @file The base file for the ieee website
 * @author Jacob Hladky
 * @namespace ieee
 */
var ieee = {
    /**
     * Container for administrative consoles
     *
     * @name ieee.consoles
     * @type object
     * @namespace ieee.consoles
     *
     */
    consoles: {},
    /**
     * Container for js associated with HTML pages
     *
     * @name ieee.pages
     * @type object
     * @namespace ieee.pages
     */
    pages: {}
};

/**
 * @function setup
 * @memberof ieee
 * @summary Base setup method for the website, called on every page.
 * @param {number} [userId] - Passed in by the jinja2 template if the
 *        user is logged in
 */
// Credit here http://homework.nwsnet.de/releases/9132/
// for the function to extend jQuery with PUT and DELETE methods
ieee.setup = function (userId) {
    $.extend({
        put: function (url, data, callback, type) {
            return utils._ajaxRequest(url, data, callback, type, "PUT");
        },
        delete_: function (url, data, callback, type) {
            return utils._ajaxRequest(url, data, callback, type, "DELETE");
        }
    });

    $(document).ajaxError(function () {
        utils.danger({
            msg: "There was an error processing your request. " +
                "Please reload the page and try again. "
        });
    });

    this.$messages = window.location.pathname.indexOf("profile") !== -1 ?
        $("#consoleMessages") : $("#messages");

    this.loaded = {};

    ///Various convenience URLs///
    this.baseUrl = window.location.protocol + "//" + window.location.host;
    this.staticUrl = "http://static.calpolyieee.org";
    this.url = "http://api.calpolyieee." +
        window.location.host.split(".").pop();
    //this.url = "http://127.0.0.1:5000";
    this.apiUrl = this.baseUrl + "/api";
    this.jUrl = this.baseUrl + "/static/js";
    ///------------------------///

    if (userId) {    
        $.get(this.apiUrl + "/users/" + userId + "/", function (data) {
            $("#profImage").attr("src", "http://www.gravatar.com/avatar/" +
                                 CryptoJS.MD5(data.profile.email) +
                                 "?s=20&d=mm");
        });
    }

    $(window).resize();
};

/**
 * @function getTaggedPosts
 * @memberof ieee
 * @summary Get all posts tagged with a tag specified in the url.
 * @param {jQuery} $ctr - HTML div to append the posts to.
 * @param {string} tag - the name of the tag
 */
ieee.getTaggedPosts = function ($ctr, tag) {
    $.get(this.apiUrl + "/posts/with_tag/" + tag, function (data) {
        $ctr.append(data.posts.map(function (post) {
            return new ieee.Post(post).render();
        }));
    });
};

/**
 * @function catPicture
 */
ieee.catPicture = function () {
    return $("<div class=\"catPicture\">").html(
        "Looks like there's nothing here. Here's a picture of a cat." +
            "<div></div>"
    );
};
