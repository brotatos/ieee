/**
 * @file Defines the Post object
 * @author Jacob Hladky
 * 
 * @constructor Post
 * @param {Object} skel - The JSON post skeleton. Sets {@link Post#skel}
 * @param {Object} [options] - A configuration object. Sets {@link Post#options}
 * @param {boolean} options.edit - Render the post with
 *        "Edit" and "Delete" buttons
 * @param {boolean} options.no_collapse - Render the post without
 *        "Collapse" or "View Alone" links
 */
ieee.Post = function (skel, options) {
    var self = this;

    /**
     *  Post options object.
     *  If none is specified an empty object is assigned.
     *  This means all options are false.
     *
     * @name Post#options
     * @type Object
     * @default {}
     */
    this.options = options || {};

    /**
     * The JSON skeleton of the Post.
     *
     * @name Post#skel
     * @type Object
     */
    this.skel = skel;

    /**
     * Whether the Post is currently expanded.
     *
     * @name Post#expanded
     * @type boolean
     */
    this.expanded = false;

    /**
     * The div containing all the HTML.
     *
     * @name Post#$frame
     * @type jQuery
     */
    this.$frame = $("<div>");

    /**
     * The main picture.
     *
     * @name Post#$pic
     * @type jQuery
     */
    this.$pic = $("<div class='pic'>");

    /**
     * The HTML content.
     *
     * @name Post#$content
     * @type jQuery
     */
    this.$content = $("<div>");

    /**
     * The top bar. Contains title, author, etc.
     *
     * @name Post#$top
     * @type jQuery
     */
    this.$top = $("<div>");

    /**
     * The mid section.
     * Contains the main pic and content.
     *
     * @name Post#$mid
     * @type jQuery
     */
    this.$mid = $("<div>");

    /**
     * The bottom bar.
     * Contains "Read More", "Share", links, etc.
     *
     * @name Post#$bottom
     * @type jQuery
     */
    this.$bottom = $("<div>");

    /**
     * Title of the post.
     *
     * @name Post#$title
     * @type jQuery
     */
    this.$title = $("<div class='title'>");

    /**
     * Author of the post.
     *
     * @name Post#$author
     * @type jQuery
     */
    this.$author = $("<a class='author'>");

    /**
     * Fuzzy time since post creation.
     *
     * @name Post#$date
     * @type jQuery
     */
    this.$date = $("<div class='date'>");

    /**
     * Any tags associated wih the post.
     *
     * @name Post#$tags
     * @type jQuery
     */
    this.$tags = $("<div class='tags'>");

    this.$expandLnk = $("<a href=\"javascript: void 0;\">").text("Read More");
    this.$collapseLnk = $("<a href=\"javascript: void 0;\">").text("Collapse");
    this.$viewLnk = $("<a href=\"javascript: void 0;\">").text("View Alone");
    this.$shareLnk = $("<a href=\"javascript: void 0;\">").text("Share");

    this.$expandLnk.add(this.$collapseLnk).click(function () {
        self.expanded = !self.expanded;
        self.render();
    });

    this.$frame.append(this.$pic, this.$content);
    this.$content.append(this.$top, this.$mid, this.$bottom);

    if (this.options.edit) {
        this.makeEditable();
    }

    if (this.options.no_collapse) {
        this.expanded = true;
        this.$expandLnk.add(this.$collapseLnk).unbind();
    }
};

/**
 * Max display length of the Post blurb.
 *
 * @name Post#BLURB_LEN
 * @type number
 * @readonly
 */
ieee.Post.BLURB_LEN = 140;

/**
 * @function makeEditable
 * @memberof Post
 * @instance
 * @summary Add "Edit" and "Delete" buttons to the Post's HTML frame
 */
ieee.Post.prototype.makeEditable = function () {
    this.$editBtn = utils.btn("Edit", "default").addClass("btn-sm");
    this.$deleteBtn = utils.btn("Delete", "danger").addClass("btn-sm");
    this.$frame.prepend(this.$editBtn, this.$deleteBtn);
};

/**
 * @function render
 * @memberof Post
 * @instance
 * @summary Turn the Post's JSON skel into HTML. Idempotent.
 * @returns {jQuery} The HTML representation of the Post
 */
ieee.Post.prototype.render = function () {
    var self = this;

    this.$viewLnk.attr("href", ieee.baseUrl + "/posts/" + this.skel.id + "/");
    this.$top.children(".small").remove();
    this.$expandLnk
        .add(this.$collapseLnk)
        .add(this.$viewLnk)
        .add(this.$shareLnk)
        .detach();

    this.$title.text(this.skel.title);
    this.$date.text($.timeago(this.skel.date * 1000));
    this.$author.text(this.skel.username);
    this.$author.attr("href", ieee.baseUrl + "/users/" +
                      this.skel.userid + "/profile/");

    if (this.expanded) {
        this.$frame.attr("class", "post expanded").prepend(this.$title);
        this.$top.html($("<div class='pull-left'>")
                       .append(this.$author, this.$date, this.$tags));
        this.$mid.html(this.skel.content);
        this.$bottom.empty().append(this.$collapseLnk, " | ",
                                    this.$viewLnk, " | ",
                                    this.$shareLnk);
        this.$top.append(this.skel.images.slice(1).map(function (href) {
            return "<a class=\"small\" href=\"" + (href || "") +
                "\" style=\"background-image: url('" + (href || "") + "');\">";
        }));
    } else {
        this.$frame.attr("class", "post");

        this.$top.empty().append(this.$title, this.$date);
        this.$mid.empty().append(this.$author, this.$tags);
        this.$bottom.empty().append($(this.skel.content).text()
                                    .substring(0, ieee.Post.BLURB_LEN) + "...",
                                    this.$expandLnk);
    }

    if (this.skel.images.length === 0 || this.skel.sticky) {
        this.$frame.addClass("noPic");
    } else {
        this.$pic.attr("style", "background-image: url('" +
                       this.skel.images[0] + "')");
    }

    self.$tags.html("Tags: ");
    if (this.skel.tags.length === 0) {
        self.$tags.append("none");
    }

    this.skel.tags.forEach(function (tag, ndx) {
        var href = tag.project && "/projects/" + tag.project ||
            "/posts/with_tag/" + tag.name;
        var newTag = "<a href=\"" + href + "\">" + tag.name + "</a>";

        if (ndx < self.skel.tags.length - 1) {
            self.$tags.append(newTag, ", ");
        } else {
            self.$tags.append(newTag);
        }
    });

    if (this.skel.sticky) {
        this.$date.detach();
        this.$top.children(".small").remove();
    }

    if (this.options.edit) {
        this.$editBtn
            .attr("href", ieee.baseUrl + "/posts/" + this.skel.id + "/edit/");
        this.$bottom.detach();
    }

    if (this.options.no_collapse) {
        this.$shareLnk.detach();
        this.$bottom.empty().append(this.$shareLnk);
    }

    return this.$frame;
};
