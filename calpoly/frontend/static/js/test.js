$(function () {    
    var files = [
        {name: "test", predef: ["qHint"]},
        {name: "ieee", predef: ["utils", "CryptoJS"]},
        {name: "Kit", predef: ["ieee", "utils"]},
        {name: "Part", predef: ["ieee", "utils"]},
        {name: "Position", predef: ["ieee", "utils"]},
        {name: "Post", predef: ["ieee", "utils"]},
        {name: "Profile", predef: ["ieee", "utils", "CryptoJS"]},
        {name: "Project", predef: ["ieee", "utils"]},
        {name: "Tag", predef: ["ieee", "utils"]},
        {name: "pages/index", predef: ["ieee", "utils"]},
        {name: "pages/officers", predef: ["ieee", "utils"]},
        {name: "pages/parts", predef: ["ieee", "utils", "Fuse"]},
        {name: "pages/posts_edit", predef: ["ieee", "utils", "nicEditors",
                                            "nicEditor"]},
        {name: "pages/projects", predef: ["ieee", "utils"]},
        {name: "pages/tutoring", predef: ["ieee", "utils"]},
        {name: "pages/users_login", predef: ["ieee", "utils"]},
        {name: "pages/current_user_profile", predef: ["ieee", "utils"]},
        {
            name: "pages/users_register", // investigate here
            predef: ["ieee", "utils", "Recaptcha", "regCaptcha"]
        },
        {name: "consoles/front_page", predef: ["ieee", "utils"]},
        {name: "consoles/posts", predef: ["ieee", "utils"]},
        {name: "consoles/profile", predef: ["ieee", "utils"]},
        {name: "consoles/tutoring", predef: ["ieee", "utils"]},
        {name: "consoles/voting", predef: ["ieee", "utils"]},
        {name: "consoles/config", predef: ["ieee", "utils"]},
        {name: "consoles/finance", predef: ["ieee", "utils", "Fuse"]}
    ];
    var options = {
        maxlen: 80,
        indent: 4,
        browser: true,
        jquery: true,
        curly: true,
        eqeqeq: true,
        freeze: true,
        immed: true,
        newcap: true,
        noarg: true,
        quotmark: "double",
        undef: true,
        unused: true,
    };

    files.forEach(function (file) {
        var opt = $.extend({}, options);

        opt.predef = file.predef;
        qHint(file.name, "/static/js/" + file.name + ".js", opt);
    });
});
