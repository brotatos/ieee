/**
 * @file Defines the Profile object
 * @author Jacob Hladky
 */

/**
 * @constructor Profile
 * @param {Object} skel - The JSON post skeleton. Sets {@link Profile#skel}
 */
ieee.Profile = function (skel) {
    /**
     * The JSON skeleton of the Profile
     *
     * @name Profile#skel
     * @type Object
     */
    this.skel = skel;

    /**
     * HTML frame for the profile
     *
     * @name Profile#$frame
     * @type jQuery
     */
    this.$frame = $("<div class='profile'>");

    /**
     * HTML tag for the profile image
     *
     * @name Profile#$image
     * @type jQuery
     */
    this.$image = $("<img class='img-circle'>");

    /**
     * Container for the body of the profile
     *
     * @name Profile#$body
     * @type jQuery
     */
    this.$body = $("<div>");

    /**
     * Profile heading.
     *
     * @name Profile#$heading
     * @type jQuery
     */
    this.$heading = $("<div class='h3'>");

    /**
     * Profile subheading.
     *
     * @name Profile#$subheading
     * @type jQuery
     */
    this.$subheading = $("<b>");

    /**
     * Facebook icon in the profile
     *
     * @name Profile#$fbicon
     * @type jQuery
     */
    this.$fbicon = $("<a href='#'>").append($("<i class='fa fa-facebook'>"));

    /**
     * Main icon in the profile.
     *
     * @name Profile#$mailicon
     * @type jQuery
     */
    this.$mailicon = $("<a href='#'>").append($("<i class='fa fa-envelope'>"));

    /**
     * Container for User bio
     *
     * @name Profile#$blurb
     * @type jQuery
     */
    this.$blurb = $("<div class='blurb'>");

    this.$body.append(this.$heading, this.$subheading, "<br>", this.$fbicon,
                      this.$mailicon,"<br><br>", this.$blurb);
    this.$frame.append(this.$image, this.$body);
};

/**
 * @function genKeyup
 * @memberof Profile
 * @instance
 * @summary generate a keyup function for the specified property
 * @returns {function} Function to get the property of the JSON skeleton
 *          to the value of the input box
 */
ieee.Profile.prototype.genKeyup = function (field) {
    var self = this;

    return function () {
        self.skel[field] = $(this).val();
        self.render();
    };
};

/**
 * @function render
 * @memberof Profile
 * @instance
 * @summary Turn the Profile's JSON skel into HTML. Idempotent.
 * @param {Object} options - An configuration object
 * @returns {jQuery} The HTML representation of the Profile
 */
ieee.Profile.prototype.render = function (options) {
    var gravatarUrl = "http://www.gravatar.com/avatar/" +
        CryptoJS.MD5(this.skel.email.trim().toLowerCase()) + "?s=150&d=mm";
    var str = this.skel.name;

    options = options || {};
    if (!options.no_title) {
        str += " - " + this.skel.title;
    }

    if (options.old) {
        str += " in " + this.skel.year_active;
    }

    this.$image.attr("src", gravatarUrl);
    this.$heading.text(str);
    this.$subheading.text(this.skel.year + " " + this.skel.major);
    this.$blurb.text(this.skel.bio);
    if (this.skel.fb_id) {
        this.$fbicon.attr("href", "http://facebook.com/" + this.skel.fb_id);
    } else {
        this.$fbicon.remove();
    }
    this.$mailicon.attr("href", "mailto:" + this.skel.email);

    return this.$frame;
};
