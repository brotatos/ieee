/**
 * @file The voting console
 * @author Jacob Hladky
 * @namespace ieee.consoles.voting
 */
ieee.consoles.voting = {};

/**
 * @function setup
 * @memberof ieee.consoles.voting
 * @summary Setup the voting console. Called once, when the console js is
 *          loaded. It is required to implement the console 'interface'.
 */
ieee.consoles.voting.setup = function (positions) {
    this.$frame = $("#votingFrame");
    this.$voteBtn = utils.btn("Vote")
        .click(this.submitBallot.bind(this))
        .addClass("btn-block");
    this.$realVoteBtn = utils.btn("Confirm Vote", "danger")
        .click(this.submitBallotForReal.bind(this));
    this.confirmVote = new utils.ModalAlert();
    this.positions = positions.map(function (position) {
        return new ieee.Position(position);
    });

    this.$frame.append(
        this.positions.map(function (p) { return p.render(); }),
        this.$voteBtn
    );
};

/**
 * @function submitBallot
 * @memberof ieee.consoles.voting
 * @summary Submit the ballot for verification and display a message to the user
 *          either prompting them to submit it for real or informing them of
 *          errors in the ballot.
 */
ieee.consoles.voting.submitBallot = function () {
    $.post(
        ieee.apiUrl + "/voting/verify/",
        JSON.stringify({ballot: this.collectBallot()}),
        this.checkBallot.bind(this)
    );
};

/**
 * @function submitBallotForReal
 * @memberof ieee.consoles.voting
 * @summary Submit the ballot after it has been verified.
 */
ieee.consoles.voting.submitBallotForReal = function () {
    var self = this;

    $.post(ieee.apiUrl + "/voting/submit/", JSON.stringify({
        ballot: this.collectBallot()
    }), utils.orAlert(function () {
        var secs = 10, $secs;

        self.confirmVote.$cancel.detach();

        utils.success({
            msg: "Ballot submitted successfully. " +
                "Thanks for voting!"
        }, self.confirmVote.$body);

        self.confirmVote.$body.append("<h4>This page will be reloaded in " +
                                      "<span id=\"s\">10</span> seconds</h4>");
        $secs = $("span#s");

        window.setInterval(function () {
            secs -= 1;
            if (secs === 0) {
                location.reload();
            }
            $secs.text(secs);
        }, 1000);
    }, this.confirmVote.$body));
};

/**
 * @function collectBallot
 * @memberof ieee.consoles.voting
 * @summary Collect the user responses into a JSON ballot, which can then
 *          be submitted for error checking. If it comes back clean, then we
 *          submit to the actual vote URL.
 */
ieee.consoles.voting.collectBallot = function () {
    this.userBallot = {};
    this.positions.forEach(function (position) {
        this.userBallot[position.skel.title] = position.getChoices();
    }, this);

    return this.userBallot;
};

/**
 * @function checkBallot
 * @memberof ieee.consoles.voting
 * @summary This is attached to the result of a POST to verify the JSON ballot.
 *          If there are any errors, it reports them in a modal alert. Otherwise
 *          it offers the user the chance to look over their ballot choices
 *          before submission.
 * @param {Object} data - The JSON data from the AJAX call
 */
ieee.consoles.voting.checkBallot = function (data) {
    var posList;

    if (data.success) {
        posList = "<ul>";
        $.each(this.userBallot, function (title, choices) {
            posList += "<li>" + title + ": ";
            posList += choices.reduce(function (str, choice, ndx) {
                return str + choice + (ndx < choices.length - 1 ? ", " : "");
            }, "") + "</li>";
        });
        posList += "</ul>";

        $("body").append(this.confirmVote.render({
            title: "Confirm Ballot Submission",
            body: "Are you sure you want to submit the following ballot?" +
                posList,
            cancel: "Cancel",
            $confirm: this.$realVoteBtn
        }));

        this.confirmVote.show();
    } else {
        utils.quickModal(
            "Error Submitting Ballot",
            "<div class=\"h4\">Your ballot had the following errors:</div>" +
                "<ul>" +
                data.errors.reduce(function (list, error) {
                    return list + "<li>" + error + "</li>";
                }, "") +
                "</ul>Please fix them and try submitting again"
        );
    }
};
