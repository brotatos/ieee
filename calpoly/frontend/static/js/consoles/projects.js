/**
 * @file The projects console
 * @author Jacob Hladky
 * @namespace ieee.consoles.projects
 */
ieee.consoles.projects = {};

/**
 * @function setup
 * @memberof ieee.consoles.projects
 * @summary Setup the projects console. Called once, when the console js is
 *          loaded. It is required to implement the console 'interface'.
 */
ieee.consoles.projects.setup = function (projects) {
    var self = this;

    this.$frame = $("#projectsFrame");
    this.$table = this.$frame.find("tbody");

    this.$table.append(projects.map(function (project) {
        var $edit = utils.btn("Edit", "default")
            .attr("href", "/projects/" + project.id + "/edit");
        var $delete = utils.btn("Delete", "danger");

        $delete.click(utils.doubleCheck("delete this project?", function () {
            var url = ieee.apiUrl + "/projects/" + project.id + "/";

            $.delete_(url, utils.orAlert(function () {
                utils.info("Project deleted");
                window.location.reload();
            }));
        }));
        
        return utils.toTr(
            project.title,
            (!project.completed && "Not " || "") + "Completed",
            $("<div>").append($edit, $delete)
        );
    }));

    if (projects.length === 0) {
        this.$frame.find(".panel-body").html(ieee.catPicture());
    }
};
