/**
 * @file The landing console
 * @author Jacob Hladky
 * @namespace ieee.consoles.front_page
 */
ieee.consoles.front_page = {};

/**
 * @function setup
 * @memberof ieee.consoles.front_page
 * @summary Setup the front_page console. Called once, when the console js is
 *          loaded. It is required to implement the console 'interface'.
 */
ieee.consoles.front_page.setup = function (events) {
    var self = this;

    this.events = events;
    this.$eventsFrame = $("#eventsFrame");
    this.$eventsTable = this.$eventsFrame.find("tbody");
    this.$newEvent = this.$eventsFrame.find(".panel-heading a.btn");
    this.eventForm = new utils.Form({
        content: utils.Form.gen.textarea("Event"),
        twitter: utils.Form.gen.checkbox("Post to Twitter")
    }, {
        url: ieee.apiUrl + "/events/",
        successAction: function (data, json) {
            json.id = data.id;
            self.events.push(json);
            self.show();
        }
    });

    this.eventForm.$container.addClass("eventForm");

    this.$newEvent.click(function () {
        utils.quickModal("New Event", self.eventForm.render());
    });

    this.show();
};

/**
 * @function _mapEvent
 * @memberof ieee.consoles.front_page
 * @summary Create a new HTML representation of an event.
 * @param {Object} evt - The event JSON skeleton
 * @param {number} ndx - The index of the event on some array.
 * @returns {jQuery} JQuery wrapped event HTML.
 */
ieee.consoles.front_page._mapEvent = function (evt, ndx) {
    var self = this;
    var $delete = utils.btn("Delete", "danger");

    $delete.click(utils.doubleCheck("delete this event?", function () {
        var url = ieee.apiUrl + "/events/" + evt.id + "/";

        $.delete_(url, utils.orAlert(function () {
            utils.info({msg: "Event deleted.", dismiss: true});
            self.events.splice(ndx, 1);
            self.show();
        }));
    }));

    return utils.toTr(evt.id, evt.content, $delete);
};

/**
 * @function show
 * @memberof ieee.consoles.front_page
 * @summary Refresh the front_page console.
 */
ieee.consoles.front_page.show = function () {
    var self = this;

    this.$eventsTable.empty().append(
        this.events.map(this._mapEvent.bind(this))
    );
};
