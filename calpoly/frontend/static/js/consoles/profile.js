/**
 * @file The profile console
 * @author Jacob Hladky
 * @namespace ieee.consoles.profile
 */
ieee.consoles.profile = {};

/**
 * @function setup
 * @memberof ieee.consoles.profile
 * @summary Setup the profile console. Called once, when the console js is
 *          loaded. It is required to implement the console 'interface'.
 */
ieee.consoles.profile.setup = function (profile) {
    var self = this, str = "delete your account? This will result in" +
        " irrevocable loss of data.";

    this.$frame = $("#profileConsole");
    this.$roles = this.$frame.find(".list-group");
    this.$deleteBtn = this.$frame.children("a:last-child");

    this.subjBasic = ["EE", "CPE", "PHYS", "MATH"];

    this.profileForm = new utils.Form({
        name: utils.Form.gen.input("Name"),
        email: utils.Form.gen.input("Email Address"),
        year: utils.Form.gen.select("Year"),
        major: utils.Form.gen.select("Major"),
        fb_id: utils.Form.gen.input("Facebook ID", "optional"),
        bio: utils.Form.gen.textarea("Bio", 3)
    }, {
        edit: true,
        url: ieee.apiUrl + "/current_user/"
    });

    this.$frame.children("hr:first-of-type").after(this.profileForm.render());

    this.profileForm.skel.year.fill(["Freshman", "Sophomore", "Junior",
                                     "Senior", "Grad Student"]);

    this.profileForm.skel.major.fill([
        "Computer Engineer",
        "Electrical Engineer",
        "Software Engineer",
        "Mechanical Engineer"
    ]);

    this.profileForm.populate(profile);
    this.profile = new ieee.Profile(profile);
    this.$frame.children("div:first-child").after(this.profile.render());

    this.$roles.append(profile.roles.map(function (role) {
        return "<a href=\"javascript: void 0;\" class=\"list-group-item\">" +
            role + "</a>";
    }));

    this.$deleteBtn.click(utils.doubleCheck(str, function () {
        $.delete_(ieee.apiUrl + "/current_user/", utils.orAlert(function () {
            utils.info("Account successfully deleted.");
            window.location.href = ieee.urlBase;
        }));
    }));

    this.profileForm.skel.name.keyup(this.profile.genKeyup("name")).keyup();
    this.profileForm.skel.bio.keyup(this.profile.genKeyup("bio")).keyup();
    this.profileForm.skel.major.change(this.profile.genKeyup("major")).change();
    this.profileForm.skel.year.change(this.profile.genKeyup("year")).change();
};
