/**
 * @file The finance console
 * @author Jacob Hladky
 * @namespace ieee.consoles.finance
 */
ieee.consoles.finance = {};

/**
 * @function setup
 * @memberof ieee.consoles.finance
 * @summary Setup the finance console. Called once, when the console js is
 *          loaded. It is required to implement the console 'interface'.
 */
ieee.consoles.finance.setup = function (reimbursements) {
    var self = this;

    ///viewing remibursement requests (rrs)///
    this.$rrs = $("#requests");
    this.$newRrBtn = this.$rrs.find(".panel-heading a.btn");
    this.rrs = reimbursements;

    this.$rrs.find(".panel-body").append(ieee.catPicture());
    this.showRrs(this.rrs);

    ///new reimbursement request///
    this.rrForm = new utils.Form({
        amount: utils.Form.gen.money("Amount"),
        type: utils.Form.gen.select("Type"),
        comment: utils.Form.gen.textarea("Comment", 4)
    }, {
        url: ieee.apiUrl + "/reimbursements/",
        successAction: function (data, json) {
            json.id = data.id;
            json.date_requested = (new Date()).getTime() / 1000;
            json.status = "PENDING";

            self.rrs.push(json);
            self.showRrs(self.rrs);
        }
    });

    this.$newRrBtn.click(function () {
        utils.quickModal("New Reimbursement Request", self.rrForm.render());
    });

    this.rrForm.skel.type.fill(["REGULAR", "CASHBOX"], true);
};

/**
 * @Function showRrs
 * @memberof ieee.consoles.finance
 * @summary Display an array of reimbursement requests (rrs) in a table.
 * @param {Object[]} rrs - An array of rrs.
 */
ieee.consoles.finance.showRrs = function (rrs) {
    this.$rrs.find("tbody").empty().append(rrs.map(function (rr) {
        return utils.arrayToTr([
            rr.id,
            utils.toMoney(rr.amount),
            utils.date(rr.date_requested),
            rr.comment,
            rr.status,
            rr.type
        ]);
    }));

    if (rrs.length === 0) {
        this.$rrs.find("table").addClass("hidden");
        this.$rrs.find(".catPicture").removeClass("hidden");
    } else {
        this.$rrs.find("table").removeClass("hidden");
        this.$rrs.find(".catPicture").addClass("hidden");
    }
};
