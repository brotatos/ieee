/**
 * @file The posts console
 * @author Jacob Hladky
 * @namespace ieee.consoles.posts
 */
ieee.consoles.posts = {};

/**
 * @function setup
 * @memberof ieee.consoles.posts
 * @summary Setup the posts console. Called once, when the console js is
 *          loaded. It is required to implement the console 'interface'.
 */
ieee.consoles.posts.setup = function (posts) {
    this.$frame = $("#postsFrame");
    this.$table = this.$frame.find("tbody");

    this.$table.append(posts.map(function (post) {
        var $edit = utils.btn("Edit", "default")
            .attr("href", "/posts/" + post.id + "/edit");
        var $delete = utils.btn("Delete", "danger");

        $delete.click(utils.doubleCheck("delete this post?", function () {
            var url = ieee.apiUrl + "/posts/" + post.id + "/";

            $.delete_(url, utils.orAlert(function () {
                utils.info("Post deleted");
                window.location.reload();
            }));
        }));
        
        return utils.toTr(
            post.title,
            utils.date(post.date),
            $("<div>").append($edit, $delete)
        );
    }));

    if (posts.length === 0) {
        this.$frame.find(".panel-body").html(ieee.catPicture());
    }
};
