/**
 * @file The run console (for users running for a position).
 * @author Jacob Hladky
 * @namespace ieee.consoles.kit_builder
 */
ieee.consoles.run = {};

/**
 * @function ieee.consoles.run.setup
 * @memberof ieee.consoles.run
 * @summary Setup the run console.
 *          Required to implement the console 'interface'.
 */
ieee.consoles.run.setup = function (user, currPositions,
                                    allPositions, questions) {
    var self = this, msg = "withdraw from this position?", descriptions;

    this.$frame = $("#runFrame");
    this.$descriptions = utils.btn("Read Position Descriptions", "default");
    this.$answersWrapper = $("<div>");
    this.questions = questions;
    this.answers = [];

    this.$answersWrapper.append(utils.Form._wrap(
        "&nbsp;",
        utils.Form.gen.p(null, "<b>Answer Questions</b>")[0]
    ));

    this.runForm = new utils.Form({
        position_id: utils.Form.gen.select("Position"),
        name: utils.Form.gen.input("Name"),
        year: utils.Form.gen.select("Class Level"),
        answers: utils.Form.gen.custom(
            this.$answersWrapper,
            this._answersVal.bind(this),
            function () {}
        ),
    }, {
        url: ieee.apiUrl + "/current_user/run/",
        title: "Run for a Position",
        successAction: function () {
            /*I'll fix this later.*/
            location.reload();
        }
    });

    this.$frame.find(".candidate > a").click(function () {
        var btn = this;

        (utils.doubleCheck(msg, function () {
            $.post(ieee.apiUrl + "/current_user/withdraw/", JSON.stringify({
                position_id: parseInt(btn.getAttribute("positionId"), 10)
            }), utils.orAlert(function () {
                utils.info({msg: "Success.", dismiss: true});
                location.reload();
            }));
        }))();
    });

    if (currPositions.length < 2) {
        this.$frame.children("h4+div+hr").after(this.runForm.render());
    }

    this.runForm.skel.year.fill(["Freshman", "Sophomore", "Junior", "Senior",
                                 "Grad Student"]);

    this.runForm.skel.name.val(user.name || "");
    if (user.year) {
        self.runForm.skel.year.val(user.year);
    }

    this.runForm.skel.position_id.fill(allPositions.map(function (posn) {
        return {val: posn.id, text: posn.title};
    }), true);

    this.questions.forEach(function (question, ndx) {
        var $answer = utils.Form.gen.textarea(),
            $label = utils.Form.gen.p(null, question.prompt);

        self.answers.push("");
        self.$answersWrapper.append(
            utils.Form._wrap("Question " + (ndx + 1), $label[0]),
            utils.Form._wrap("Answer", $answer[0])
        );

        $answer.keyup(function () {
            self.answers[ndx] = $(this).val();
        });
    });

    descriptions = allPositions.reduce(function (str, position) {
        return str + "<h4>" + position.title + "</h4>" + position.description;
    }, "");

    this.$descriptions.appendTo(this.runForm.$title.children("h4"));

    this.$descriptions.click(function () {
        utils.quickModal("Position Descriptions", descriptions);
    });
};

/**
 * @function ieee.consoles.run._answersVal
 * @private
 * @memberof ieee.consoles.run
 * @summary Return an array of the candidates answers.
 */
ieee.consoles.run._answersVal = function () {
    var self = this;

    //We don't have to worry about setting here, this form is never populated
    return this.questions.map(function (question, ndx) {
        return {prompt: question.prompt, response: self.answers[ndx]};
    });
};
