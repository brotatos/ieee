/**
 * @file The config console.
 * @author Jacob Hladky
 * @namespace ieee.consoles.config
 */
ieee.consoles.config = {};

/**
 * @function setup
 * @memberof ieee.consoles.config
 * @summary Setup the config console. Called once, when the console js is
 *          loaded. It is required to implement the console 'interface'.
 */
ieee.consoles.config.setup = function (config) {
    var self = this, key, msg = "reset ALL voting tables? This will result " +
        "in irrevocable loss of data.";

    this.$console = $("#configConsole > #console");
    this.$form = $("#configConsole > div.form-horizontal");
    this.$submitBtn = $("#configConsole > #submit");
    this.$resetVotingBtn = $("#configConsole > #reset");
    this.$processVotingBtn = $("#configConsole > #process");

    this.config = config;
    for (key in this.config) {
        if (this.config.hasOwnProperty(key)) {
            this.handle.call(self, key, this.config[key]);
        }
    }

    this.$submitBtn.click(function () {
        var url = ieee.apiUrl + "/config/";

        $.put(url, JSON.stringify(self.config), utils.orAlert(function () {
            utils.success({msg: "Submit successful.", dismiss: true});
        }));
    });

    this.$resetVotingBtn.click(utils.doubleCheck(msg, function () {
        $.delete_(ieee.apiUrl + "/voting/", utils.orAlert(function () {
            utils.success({msg: "Reset Successful", dismiss: true});
        }));
    }));

    this.$processVotingBtn.click(function () {
        $.get(ieee.apiUrl + "/voting/results/", utils.orAlert(function (data) {
            var str = "<ul>";

            $.each(data.results, function (pos, val) {
                str += "<li>" + pos + " : "  + JSON.stringify(val) + "</li>";
            });

            self.$console.append(
                "Voting Totals:",
                str,
                "Write Ins:",
                data.write_ins.reduce(function (list, writeIn) {
                    return list += "<li>" + writeIn + "</li>";
                }, "<ul>")
            );
        }));
    });
};

/**
 * @function handle
 * @memberof ieee.consoles.config
 * @summary Generate an input element in the config form for the property.
 *          Add a keyup to this input to set the config Object to the value
 *          of the input whenever it changes.
 * @param {string} key - The name of the config variable.
 * @param {*} value - The config variable. This function will make a different
 *        input element for boolean and date variables (checkbox and HTML5
 *        datepicker, respectively). It otherwise defaults to a text input.
 */
ieee.consoles.config.handle = function (key, value) {
    var self = this, $control;

    if (typeof value === "boolean") {
        $control = utils.Form.gen.checkbox();
        $control.change(function () {
            self.config[key] = this.checked;
        });
        $control[0].checked = value;
    } else if (key.indexOf("date") !== -1) {
        $control = utils.Form.gen.date();
        $control.change(function () {
            self.config[key] = new Date($(this).val()).getTime() / 1000;
        });
        $control[0].value = new Date(parseInt(value, 10) * 1000)
            .toJSON().substr(0, 10);
    } else {
        $control = utils.Form.gen.input();
        $control.keyup(function () {
            self.config[key] = $(this).val();
        });
        $control.val(value);
    }

    self.$form.append(utils.Form._wrap(key, $control[0]));
};
