/**
 * @file Defines the Part object
 * @author Jacob Hladky
 */

/**
 * @constructor Part
 * @memberof ieee
 * @param {Object} skel - The JSON post skeleton. Sets {@link Part#skel}
 */
ieee.Part = function (skel) {
    /**
     * The JSON skeleton of the Part.
     *
     * @name Part#skel
     * @type Object
     */
    this.skel = skel;
};

/**
 * @function toString
 * @memberof Part
 * @instance
 * @summary Override the builtin string representation of this object.
 * @returns {string} The text representation of the part
 */
ieee.Part.prototype.toString = function () {
    return this.skel.category + " " +
        ieee.Part.toHuman(this.skel.main_info) + " " +
        this.skel.addtl_info.reduce(function (str, pair) {
            return str + ieee.Part.toHuman(pair) + " ";
        }, "") + " " +
        (this.skel.chip_type || "") + " " +
        (this.skel.mftr || "") + " " +
        (this.skel.name || "") + " ";
};

/**
 * @function toSymbol
 * @memberof Part
 * @summary Convert an SI unit to a HTML symbol. If the unit is not
 *          in the symbol table it is passed through without modification.
 * @param {string} unit - An SI unit.
 * @returns {string} HTML symbol for the unit
 */
ieee.Part.toSymbol = function (unit) {
    switch (unit) {
    case "Ohm": return "&Omega;";
    case "Farad": return "F";
    case "Watt": return "W";
    case "Hertz": return "Hz";
    default: return unit;
    }
};

/**
 * @function toHuman
 * @memberof Part
 * @summary Convert a Name-Value pair, representing a property
 *          of the Part object, into a human readable string.
 *          Values will be returned in engineering notation.
 * @param {Object} pair - The Name-Value pair
 * @param {string} pair.name - The "name" of the pair. Probably a unit.
 * @param {number} [pair.value] - The "value" of the pair
 * @returns {string} Human readable string for the Name-Value pair
 */
ieee.Part.toHuman = function (pair) {
    //positive magnitude prefixes
    var posMags = ["", "k", "M", "G", "T", "P"];
    //negative magnitude prefixes
    var negMags = ["", "m", "&micro;", "n", "p", "f", "a"];
    var mag = 0, unit, value;

    if(!pair.hasOwnProperty("value")) {
        return pair.name;
    }

    value = pair.value = parseFloat(pair.value);

    if (pair.value && pair.value > 999) {
        while (value > 1000) {
            value /= 1000;
            mag++;
        }
        unit = posMags[mag] + ieee.Part.toSymbol(pair.name);
    } else if(pair.value && pair.value < 1) {
        while (value < 1) {
            value *= 1000;
            mag++;
        }
        unit = negMags[mag] + ieee.Part.toSymbol(pair.name);
    } else {
        unit = ieee.Part.toSymbol(pair.name);
    }

    //the extra '+' before value removes trailing zeros
    return +value.toFixed(2) + unit;
};
