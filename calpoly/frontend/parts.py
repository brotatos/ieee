from sqlalchemy import distinct
from flask import Blueprint, render_template
from calpoly.core import db
from calpoly.users.decorators import login_required
from calpoly.parts.models import Part, Kit

mod = Blueprint('parts', __name__, url_prefix='/parts')


def get_categories():
    return [i[0] for i in db.session.query(distinct(Part.category)).all()]


@mod.route('/')
def get_all():
    return render_template(
        'parts.html',
        categories=get_categories(),
        kits=[kit.serialize for kit in Kit.query.all()]
    )


@mod.route('/<int:id>/location')
@login_required
def parts_location(id):
    return render_template('parts_location.html',
                           part=Part.query.get_or_404(id))
