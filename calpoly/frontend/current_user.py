import datetime
from flask import Blueprint, render_template
from flask.ext.login import current_user
from calpoly.users.decorators import login_required, requires_roles
from calpoly.voting.utils import get_results
from calpoly.config import DEBUG
from calpoly.core_models.models import Config
from calpoly.voting.models import Position, Question
from calpoly.projects.models import Project
from calpoly.events.models import Event

mod = Blueprint('current_user', __name__, url_prefix='/current_user')


@mod.route('/profile/')
@login_required
def home():
    return render_template(
        'current_user/profile.html',
        user=current_user,
        debug=bool(DEBUG),
        unverified='unverified' in current_user.get_roles()
    )


@mod.route('/profile/run/')
@login_required
def run_console():
    return render_template(
        'consoles/run.html',
        current_user=current_user.serialize,
        current_positions=[i.serialize for i in current_user.positions],
        all_positions=[i.serialize for i in Position.query.all()],
        questions=[i.serialize for i in Question.query.all()]
    )


@mod.route('/profile/voting/')
@login_required
@requires_roles('voter')
def voting_console():
    now = datetime.datetime.now()
    config = Config.query.first()
    errors = []

    if now < config.voting_end_date and not config.voting_started:
        errors.append("The voting period has not opened yet")

    if not errors and config.voting_started and current_user.voted:
        errors.append("You have already voted. Thank you!")

    if not errors and now > config.voting_end_date:
        errors.append("The voting period has ended")

    if current_user.voted:
        results, write_ins, totals = get_results()
    else:
        results, write_ins, totals = (None, None, None)

    return render_template(
        'consoles/voting.html',
        success=not errors,
        errors=errors,
        positions=[i.serialize for i in Position.query.all()],
        end_date=Config.query.first().voting_end_date,
        results=results,
        write_ins=write_ins,
        totals=totals
    )


@mod.route('/profile/profile/')
@login_required
@requires_roles('officer')
def profile_console():
    return render_template(
        'consoles/profile.html',
        current_user=current_user.serialize
    )


@mod.route('/profile/finance/')
@login_required
@requires_roles('officer')
def finance_console():
    return render_template(
        'consoles/finance.html',
        reimbursements=[i.serialize for i in current_user.reimbursements]
    )


@mod.route('/profile/posts/')
@login_required
@requires_roles('poster')
def posts_console():
    return render_template(
        'consoles/posts.html',
        posts=[i.serialize for i in current_user.posts]
    )


@mod.route('/profile/projects/')
@login_required
@requires_roles('secretary')
def projects_console():
    return render_template(
        'consoles/projects.html',
        projects=[i.serialize for i in Project.query.all()]
    )


@mod.route('/profile/front_page/')
@login_required
@requires_roles('publicity')
def front_page_console():
    return render_template(
        'consoles/front_page.html',
        events=[i.serialize for i in Event.query.all()]
    )


@mod.route('/profile/config/')
@login_required
@requires_roles('webmaster')
def config_console():
    return render_template(
        'consoles/config.html',
        config=Config.query.first().serialize
    )
