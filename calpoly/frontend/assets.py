# -*- coding: utf-8 -*-
"""
    calpoly.frontend.assets
    ~~~~~~~~~~~~~~~~~~~~~~~

    Frontend asset setup for minification purposes.
"""

from flask.ext.assets import Environment, Bundle
css_all = Bundle("css/*", filters="cssmin", output="css/gen/ieee.min.css")


def init_webassets(app):
    web_assets = Environment(app)
    web_assets.register('css_all', css_all)
