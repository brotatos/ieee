from calpoly.core import db
from flask.ext.superadmin import AdminIndexView
from flask.ext.superadmin.model import ModelAdmin
from flask.ext.login import current_user


class MyAdminView(ModelAdmin):

    session = db.session

    def is_accessible(self):
        return current_user.is_authenticated() \
            and 'webmaster' in current_user.get_roles()


class MyAdminIndexView(AdminIndexView):
    def is_accessible(self):
        return current_user.is_authenticated() \
            and 'webmaster' in current_user.get_roles()
