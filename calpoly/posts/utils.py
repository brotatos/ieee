import re
import jinja2
import scrubber

TAG_REGEX = re.compile(r'<[^>]+>')


def sanitize(text):
    """
    Remove any harmful html by sanitizing it.
    """
    return jinja2.Markup(scrubber.Scrubber().scrub(text))


def _remove_html_tags(html):
    """
    Performs a regex substitution to remove html tags from the html param.
    e.g. '<p> foobar </p>' becomes 'foobar'
    """
    return TAG_REGEX.sub('', html)


def has_content(post_content):
    """
    Removes html tags in `post_content` and strips all whitespace from
    the post.
    """
    return bool(_remove_html_tags(post_content).replace(" ", ""))
