from calpoly.core import db
from calpoly.core_models.models import ImageLink
from calpoly.users.models import User
from calpoly.projects.models import Project
from calpoly.api.utils import get_or_create, unix_time
from calpoly.posts.utils import sanitize
from calpoly.models import register_to_admin


post_tags_table = db.Table('post_tags', db.Model.metadata,
                           db.Column('post_id', db.Integer,
                                     db.ForeignKey('post.id')),
                           db.Column('tag_id', db.Integer,
                                     db.ForeignKey('tag.id')),
                           )

post_imagelink_table = db.Table('post_imagelink', db.Model.metadata,
                                db.Column('post_id', db.Integer,
                                          db.ForeignKey('post.id')),
                                db.Column('imagelink_id', db.Integer,
                                          db.ForeignKey('imagelink.id')),
                                )


@register_to_admin
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120))
    content = db.Column(db.Text, nullable=False)
    date = db.Column(db.BigInteger, nullable=False)
    sticky = db.Column(db.Boolean())
    user = db.relationship('User')
    user_id = db.Column(db.Integer, db.ForeignKey(User.id))
    images = db.relationship('ImageLink', secondary=post_imagelink_table)
    tags = db.relationship('Tag', secondary=post_tags_table)

    def __repr__(self):
        return '<Post %r>' % (self.title)

    def __init__(self, json, user_id):
        self.date = unix_time()
        self.title = json['title']
        self.content = sanitize(json['content'])
        self.sticky = False
        self.user_id = user_id
        self.tags = [get_or_create(tag['name'], Tag, name=tag['name'])
                     for tag in json['tags']]
        self.images = [get_or_create(link, ImageLink, link=link)
                       for link in json['images']]

    @property
    def serialize(self):
        return {
            'id':            self.id,
            'title':         self.title,
            'content':       self.content,
            'date':          self.date,
            'sticky':        self.sticky,
            'username':      self.user.name,
            'userid':        self.user.id,
            'tags':          [i.serialize for i in self.tags],
            'images':        [i.serialize for i in self.images]
        }

    @staticmethod
    def sortable():
        return ['id', 'title', 'date', 'user_id']


@register_to_admin
class Tag(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Unicode(64), nullable=False)

    def __init__(self, name=None):
        self.name = name

    def __unicode__(self):
        return self.name

    def __repr__(self):
        return '<Tag %r>' % self.name

    @staticmethod
    def sortable():
        return ['id', 'name']

    @property
    def serialize(self):
        json = {
            'name': self.name
        }
        project = Project.query.filter_by(associated_tag=self).first()

        if project:
            json['project'] = project.url
        return json
