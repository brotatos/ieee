from functools import wraps
from flask import flash, redirect, url_for, request
from flask.ext.login import current_user


def requires_roles(*roles):
    """
    Decorator that allows certain user roles to a view.

    Example usage:
    @require_roles('admin') # Now only admins can view foo.
    @require_roles('admin', 'staff') # Only admins and staff can view this.
    """
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if not current_user.is_authenticated() or \
                not any([True for role in current_user.get_roles() if role in
                         roles]):
                flash(u'You are not authorized to view this page.',
                      'danger')
                return redirect(url_for('users.login', next=request.path))
            return f(*args, **kwargs)
        return wrapped
    return wrapper


def login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.is_authenticated():
            flash(u'You must be logged in to view this page.',
                  'danger')
            return redirect(url_for('users.login', next=request.path))
        return func(*args, **kwargs)
    return decorated_view
