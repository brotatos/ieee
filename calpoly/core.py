# -*- coding: utf-8 -*-
"""
    calpoly.core
    ~~~~~~~~~~~~

    Core module containing objects/classes needed throughout the app.
"""

from flask.ext.mail import Mail
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.login import LoginManager
from flask.ext.gravatar import Gravatar
from flask.ext.compress import Compress

# Flask-Sqlalchemy instance.
db = SQLAlchemy()

# Flask-Mail instance.
mail = Mail()

# Flask-Login instance.
login_manager = LoginManager()
# Not sure where to place this in the application factories.
login_manager.login_view = 'index'

# Flask-Gravatar instance.
gravatar = Gravatar(size=150, rating='x', default='blank')

# Flask-Compress instance.
compress = Compress()
