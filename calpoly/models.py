# -*- coding: utf-8 -*-
"""
    calpoly.models
    ~~~~~~~~~~~~~~~

    Obtains all models needed for admin registration using a decorator.
"""
admin_objects = set()


def register_to_admin(cls):
    """A class decorator that appends classes to the set `ADMIN_OBJECTS`. The
    set is intended for use with Flask-SuperAdmin's `register` method.

    :param cls: The class to add to the set.
    """
    admin_objects.add(cls)
    return cls
