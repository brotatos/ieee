from calpoly.core import db
from calpoly.users.models import User
from calpoly.api.utils import unix_time


class Reimbursement(db.Model):

    __tablename__ = 'reimbursement'
    id = db.Column(db.Integer, primary_key=True)
    requester = db.relationship('User')
    requester_id = db.Column(db.Integer, db.ForeignKey(User.id))
    amount = db.Column(db.Numeric(precision=10, scale=4))
    comment = db.Column(db.Text(convert_unicode=True),
                        nullable=False, unique=False)
    date_requested = db.Column(db.DateTime())
    date_approved = db.Column(db.DateTime())
    type = db.Column(db.Enum('REGULAR', 'CASHBOX',
                             name='reimbursement_type'))
    status = db.Column(db.Enum('PENDING', 'APPROVED', 'COMPLETED',
                               name='reimbursement_status'))

    def __init__(self, requester_id=None, amount=None, comment=None, type=None,
                 date_requested=None, date_approved=None, status='PENDING'):
        self.requester_id = requester_id
        self.amount = amount
        self.comment = comment
        self.type = type
        self.date_requested = date_requested
        self.date_approved = date_approved
        self.status = status

    def __repr__(self):
        return '<Reimbursement Date: %r Amount: %r>' % \
            (str(self.date_requested), str(self.amount))

    @staticmethod
    def sortable():
        return ['id', 'date_requested', 'date_approved', 'type',
                'status', 'amount']

    @property
    def serialize(self):
        if self.date_approved:
            date = unix_time(self.date_approved)
        else:
            date = ''

        return {
            'id': self.id,
            'requester_id': self.requester_id,
            'requester_name': User.query.filter_by(id=self.requester_id)
            .first().name,
            'amount': int(self.amount * 100),
            'comment': self.comment,
            'type': self.type,
            'date_requested': unix_time(self.date_requested),
            'date_approved': date,
            'status': self.status
        }
