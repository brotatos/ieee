import urllib
import os
from twython import Twython, TwythonError
from config import TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, \
    TWITTER_ACCESS_TOKEN_KEY, TWITTER_ACCESS_TOKEN_SECRET

# 'verify' key in client_args turns SSL verification off.
twitter = Twython(app_key=TWITTER_CONSUMER_KEY,
                  app_secret=TWITTER_CONSUMER_SECRET,
                  oauth_token=TWITTER_ACCESS_TOKEN_KEY,
                  oauth_token_secret=TWITTER_ACCESS_TOKEN_SECRET,
                  client_args={'verify': False})


def tweet(message, errors):
    # Check for an empty string.
    if message:
        try:
            twitter.update_status(status=message)
        except TwythonError as inst:
            errors.append(inst.message)


def upload_photo_to_twitter(image_url, errors, message=None):
    success = True
    filename = "temp"

    try:
        urllib.urlretrieve(image_url, filename=filename)
    except IOError:
        errors.append('Invalid url.')
        success = False

    if success:
        photo = open(filename, 'rb')

        if not message:
            message = 'Check this out @ 20-115.'

        try:
            twitter.update_status_with_media(status=message, media=photo)
        except TwythonError as inst:
            errors.append(inst.message)

        os.remove(filename)

    return success
