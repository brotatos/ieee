from flask import Blueprint, request, jsonify
from calpoly.core_models.models import Config
from calpoly.api.decorators import api_login_required, api_requires_roles, \
    api_requires_keys

mod = Blueprint('config', __name__, url_prefix='/config')


@mod.route('/', methods=['GET'])
@api_login_required
@api_requires_roles('webmaster')
def get():
    return jsonify(success=True, config=Config.query.first().serialize)


@mod.route('/', methods=['PUT'])
@api_login_required
@api_requires_roles('webmaster')
@api_requires_keys('voting_started', 'lounge_open', 'voting_end_date',
                   'landing_image', 'officers_image')
def edit():
    Config.query.first().edit(request.get_json(force=True))
    return jsonify(success=True)
