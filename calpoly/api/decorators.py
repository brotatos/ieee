from functools import wraps
from flask.ext.login import current_user
from flask import request, jsonify


def api_login_required(func):
    @wraps(func)
    def decorated_view(*args, **kwargs):
        if not current_user.is_authenticated():
            return jsonify(success=False,
                           errors=["Not authorized: login required"])
        return func(*args, **kwargs)
    return decorated_view


def api_requires_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if not current_user.is_authenticated() or \
                not any([True for role in current_user.get_roles() if role in
                         roles]):
                return jsonify(success=False,
                               errors=["Not authorized: requires role " +
                                       roles[0]])
            return f(*args, **kwargs)
        return wrapped
    return wrapper


def api_requires_keys(*keys):
    def wrapper(func):
        @wraps(func)
        def wrapped(*args, **kwargs):
            errors = []
            json = request.get_json(force=True)
            key_set = set(keys)

            if not key_set.issubset(set(json.keys())):
                missing = key_set.difference(set(json.keys()))
                for expected in missing:
                    errors.append("`" + expected + "` must be specified")
                return jsonify(success=False, errors=errors)
            else:
                return func(*args, **kwargs)
        return wrapped
    return wrapper
