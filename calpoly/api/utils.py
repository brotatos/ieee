import datetime

from sqlalchemy import desc
from sqlalchemy.exc import IntegrityError, InvalidRequestError
from calpoly.core import db


def unix_time(dt=datetime.datetime.now()):
    """
    Convert a datetime object to unix epoch
    If no argument is specified gives the current epoch time
    """
    epoch = datetime.datetime.utcfromtimestamp(0)
    delta = dt - epoch
    return delta.total_seconds()


def is_number(s):
    """
    Checks if a string is representing a number.
    e.g. '5' '123' "43"
    """
    try:
        float(s)
        return True
    except ValueError:
        return False


def is_int(s):
    try:
        int(s)
        return True
    except ValueError:
        return False


def get_or_create(json, model, **kwargs):
    instance = db.session.query(model).filter_by(**kwargs).first()

    if instance:
        return instance
    else:
        instance = model(json)
        db.session.add(instance)
        db.session.commit()
        return instance


def request_opts(request):
    opts = {
        'desc': False,
        'sort': request.args.get('sort'),
        'limit': request.args.get('limit'),
        'filter_param': {}
    }

    if opts['sort'] and opts['sort'][0] == "-":
        opts['desc'] = True
        opts['sort'] = opts['sort'][1:]

    for arg in request.args:
        if arg not in ['sort', 'limit']:
            opts['filter_param'][arg] = request.args.get(arg)

    return opts


def get_items(container, model, opts):
    response = {'errors': []}
    order_param = None
    limit_param = None

    if opts['sort'] and opts['sort'] not in model.sortable():
        response['errors'].append('Invalid sort parameter.')
    elif opts['sort']:
        order_param = desc(getattr(model, opts['sort'])) if opts['desc']\
            else getattr(model, opts['sort'])

    if opts['limit'] and not is_int(opts['limit']):
        response['errors'].append('Invalid limit parameter.')
    elif opts['limit']:
        limit_param = int(opts['limit'])

    if not response['errors']:
        try:
            response[container] = [i.serialize for i in
                                   db.session.query(model)
                                   .filter_by(**opts['filter_param'])
                                   .order_by(order_param).limit(limit_param)]
        except InvalidRequestError:
            response['errors'].append('Invalid filter parameter.')

    response['success'] = not response['errors']
    return response


def get_item(item_id, container, model, user=None):
    response = {"success": True, "errors": []}
    item = db.session.query(model).get(item_id)

    if not item:
        response['success'] = False
        response['errors'].append("Does not exist.")
    elif user and\
            item.user_id != user.id and\
            'webmaster' not in user.get_roles():
        response['success'] = False
        response['errors'].append("Forbidden.")
    else:
        response[container] = item.serialize
    return response


def delete_item(item_id, model, user=None):
    response = {"success": True, "errors": []}
    item = db.session.query(model).get(item_id)

    if not item:
        response['success'] = False
        response['errors'].append("Does not exist.")
    elif user and\
            item.user_id != user.id and\
            'webmaster' not in user.get_roles():
        response['success'] = False
        response['errors'].append("Forbidden.")
    else:
        try:
            db.session.delete(item)
            db.session.commit()
        except IntegrityError:
            response['success'] = False
            response['errors'].append("Foreign key dependency.")
    return response
