from flask import Blueprint, request, jsonify
from calpoly.core import db
from calpoly.users.models import User
from calpoly.posts.models import Tag
from calpoly.projects.models import Project
from calpoly.posts.utils import sanitize, has_content
from calpoly.api.utils import delete_item, get_items, request_opts, \
    get_or_create, get_item
from calpoly.api.decorators import api_login_required, \
    api_requires_roles, api_requires_keys


mod = Blueprint('projects', __name__, url_prefix='/projects')


@mod.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('projects', Project, request_opts(request)))


@mod.route('/<int:id>/', methods=['GET'])
def get(id):
    return jsonify(get_item(id, 'project', Project))


@mod.route('/<int:id>/', methods=['DELETE'])
@api_login_required
@api_requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, Project))


@mod.route('/<int:id>/', methods=['PUT'])
@api_login_required
@api_requires_roles('officer')
@api_requires_keys('title', 'completed', 'repo', 'contact_id', 'image',
                   'caption', 'description', 'content')
def edit_project(id):
    json = request.get_json(force=True)
    errors = []
    project = Project.query.get(id)
    contact = User.query.get(json['contact_id'])
    prj_url = json['title'].replace(' ', '_').lower()
    prj_tag = get_or_create(prj_url, Tag, name=prj_url)

    if project is None:
        errors.append("Does not exist.")

    if contact is None:
        errors.append('Project contact does not exist.')

    if prj_url == '':
        errors.append('Invalid project title.')

    if not has_content(json['content']):
        errors.append('Project must have content.')

    if not errors:
        project.title = json['title']
        project.completed = json['completed']
        project.repo = json['repo']
        project.contact = contact
        project.image_link = json['image']
        project.image_caption = json['caption']
        project.description = json['description']
        project.content = sanitize(json['content'])
        project.url = prj_url
        project.associated_tag = prj_tag
        db.session.commit()

    return jsonify(success=not errors, errors=errors,
                   url=prj_url, tag=prj_tag.serialize)


@mod.route('/', methods=['POST'])
@api_login_required
@api_requires_roles('officer')
@api_requires_keys('title', 'completed', 'repo', 'contact_id', 'image',
                   'caption', 'description', 'content')
def new_project():
    json = request.get_json(force=True)
    errors = []
    contact = User.query.get(json['contact_id'])
    prj_url = json['title'].replace(' ', '_').lower()
    prj_tag = get_or_create(prj_url, Tag, name=prj_url)

    if contact is None:
        errors.append('Project contact does not exist.')

    if prj_url == '':
        errors.append('Invalid project title.')

    if Project.query.filter_by(url=prj_url).first():
        errors.append('A project already exists with that title')

    if not has_content(json['content']):
        errors.append('Project must have content.')

    if not errors:
        project = Project(
            title=json['title'],
            completed=json['completed'],
            repo=json['repo'],
            contact=contact,
            image=json['image'],
            caption=json['caption'],
            description=json['description'],
            content=sanitize(json['content']),
            url=prj_url,
            tag=prj_tag
        )
        db.session.add(project)
        db.session.commit()

    return jsonify(success=not errors, errors=errors,
                   url=prj_url, tag=prj_tag.serialize)
