from flask import Blueprint, request, jsonify
from calpoly.core import db
from calpoly.events.models import Event
from calpoly.api.utils import delete_item, get_items, request_opts
from calpoly.api.decorators import api_login_required, api_requires_roles, \
    api_requires_keys
from calpoly.twitterBot import tweet

mod = Blueprint('events', __name__, url_prefix='/events')


@mod.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('events', Event, request_opts(request)))


@mod.route('/<int:id>/', methods=['DELETE'])
@api_login_required
@api_requires_roles('publicity')
def delete(id):
    return jsonify(delete_item(id, Event))


@mod.route('/', methods=['POST'])
@api_login_required
@api_requires_roles('publicity')
@api_requires_keys('content', 'twitter')
def new():
    errors = []
    json = request.get_json(force=True)
    event_id = None

    if json['content'] == "":
        errors.append("Empty event not allowed.")

    if not errors:
        if json['twitter']:
            tweet(json['content'], errors=errors)

        if not errors:
            event = Event(json['content'])
            db.session.add(event)
            db.session.commit()
            event_id = event.id

    return jsonify(success=not errors, errors=errors, id=event_id)
