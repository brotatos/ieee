from flask import Blueprint, request, jsonify
from flask.ext.login import current_user
from calpoly.api.utils import get_or_create, delete_item, get_item, get_items,\
    request_opts
from calpoly.core import db
from calpoly.posts.models import Post, Tag, ImageLink
from calpoly.posts.utils import sanitize, has_content
from calpoly.api.decorators import api_login_required, api_requires_roles, \
    api_requires_keys

mod = Blueprint('posts', __name__, url_prefix='/posts')


@mod.route('/', methods=['GET'])
def get_container():
    return jsonify(get_items('posts', Post, request_opts(request)))


@mod.route('/<int:id>/', methods=['GET'])
def get(id):
    return jsonify(get_item(id, 'post', Post))


@mod.route('/<int:id>/', methods=['PUT'])
@api_login_required
@api_requires_roles('poster')
@api_requires_keys('title', 'content', 'tags', 'images')
def edit_post(id):
    errors = []
    json = request.get_json(force=True)
    post = Post.query.get(id)

    if not post:
        errors.append("Post does not exist.")
    elif post.user_id != current_user.id:
        errors.append("Forbidden.")

    if not errors:
        post.title = json['title']
        post.content = sanitize(json['content'])
        post.tags = [get_or_create(tag['name'], Tag, name=tag['name'])
                     for tag in json['tags']]
        post.images = [get_or_create(link, ImageLink, link=link)
                       for link in json['images']]
        db.session.commit()
    return jsonify(success=not errors, errors=errors)


@mod.route('/<int:id>/', methods=['DELETE'])
@api_login_required
@api_requires_roles('poster')
def delete(id):
    return jsonify(delete_item(id, Post, user=current_user))


# No way to implement this with get_items right now
# But we might be able to get rid of it with the new way we're doing things
@mod.route('/recent/<int:max_id>/', methods=['GET'])
def get_last_ten_posts(max_id=None):
    # The minimum id: grab the last ten posts so substract it from the max_id
    return jsonify(posts=[i.serialize for i in Post.query
                          .order_by(Post.date.desc())
                          .filter(Post.id >= max_id - 10, Post.id < max_id,
                                  Post.sticky == 'f')])


# Convenience magazine
@mod.route('/recent/', methods=['GET'])
def get_recent_posts():
    """
    Returns the latest 10 posts in ordered by descending date
    """
    return jsonify(get_items('posts', Post, dict(
        desc=True, sort='date', limit=10, filter_param={'sticky': 'false'}
    )))


# No way to do this with get_items right now
@mod.route('/with_tag/<string:tag>/', methods=['GET'])
def get_posts_by_tag(tag):
    return jsonify(posts=[i.serialize for i in Post.query
                          .order_by(Post.date.desc())
                          .filter(Post.tags.any(name=tag))])


@mod.route('/', methods=['POST'])
@api_login_required
@api_requires_roles('poster')
@api_requires_keys('title', 'content', 'tags', 'images')
def new_post():
    errors = []
    json = request.get_json(force=True)
    post = Post(json, current_user.id)

    if not has_content(post.content):
        errors.append("Can't create empty post.")

    if not errors:
        db.session.add(post)
        db.session.commit()
    return jsonify(success=not errors, errors=errors)
