import datetime
from flask import Blueprint, request, jsonify
from flask.ext.login import current_user
from calpoly.core import db
from calpoly.finance.models import Reimbursement
from calpoly.api.decorators import api_login_required, api_requires_roles, \
    api_requires_keys

mod = Blueprint('reimbursements', __name__, url_prefix='/reimbursements')
STATUSES = ['PENDING', 'APPROVED', 'COMPLETED']
TYPES = ['REGULAR', 'CASHBOX']


@mod.route('/', methods=['POST'])
@api_login_required
@api_requires_roles('officer')
@api_requires_keys('amount', 'type', 'comment')
def new_reimbursement():
    errors = []
    json = request.get_json(force=True)
    request_id = None

    if not json['amount'] or int(json['amount']) <= 0:
        errors.append('Amount must be a positive value. ' +
                      'Include both dollars and cents.')

    if json['type'] not in TYPES:
        errors.append('Invalid type.')

    if not json['comment'].strip():
        errors.append("Comment cannot be an empty string")

    if not errors:
        reimbursement = Reimbursement(
            requester_id=current_user.id,
            amount=float(json['amount']) / 100,
            type=json['type'],
            comment=json['comment'],
            date_requested=datetime.datetime.now()
        )

        db.session.add(reimbursement)
        db.session.commit()
        request_id = reimbursement.id

    return jsonify(success=not errors, errors=errors, id=request_id)
