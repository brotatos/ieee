from flask import Blueprint, request, jsonify
from flask.ext.login import current_user
from sqlalchemy.exc import DataError
from calpoly.core import db
from calpoly.api.utils import delete_item
from calpoly.api.decorators import api_login_required, api_requires_roles, \
    api_requires_keys
from calpoly.users.models import User
from calpoly.voting.models import Position, Question, Answer
from calpoly.core_models.models import Config

mod = Blueprint('current_user', __name__, url_prefix='/current_user')


@mod.route('/', methods=['DELETE'])
@api_login_required
@api_requires_roles('delete_eligible')
def delete():
    return jsonify(delete_item(current_user.id, User))


@mod.route('/', methods=['PUT'])
@api_login_required
@api_requires_roles('officer')
@api_requires_keys('year', 'major', 'fb_id', 'bio', 'name')
def edit_profile():
    json = request.get_json(force=True)
    errors = []

    current_user.bio = json['bio']
    current_user.year = json['year']
    current_user.major = json['major']
    current_user.facebookid = json['fb_id']
    current_user.name = json['name']

    db.session.commit()
    return jsonify(success=not errors, errors=errors)


# Run for a position, for officer elections
@mod.route('/run/', methods=['POST'])
@api_login_required
@api_requires_keys('position_id', 'answers', 'name', 'year')
def run():
    errors = []
    json = request.get_json(force=True)

    try:
        position = Position.query.get(json['position_id'])
    except DataError:
        position = None

    if position is None:
        errors.append('Position does not exist.')

    if not errors:
        for j_ans in json['answers']:
            question = Question.query.filter_by(prompt=j_ans['prompt']).first()

            if question is None or 'response' not in j_ans.keys() \
                    or not j_ans['response']:
                errors.append('Invalid response.')
                continue

            answer = Answer(question_id=question.id,
                            user_id=current_user.id,
                            position_id=position.id,
                            answer=j_ans['response'])
            db.session.add(answer)

    if not errors:
        current_user.name = json['name']
        current_user.year = json['year']

    if not errors and len(current_user.positions) >= 2:
        errors.append('You cannot run for more than 2 positions.')

    if not errors and position.name == 'Branch President' and \
            'officer' not in current_user.get_roles():
        errors.append('You must be a current officer to run for President')

    if not errors:
        current_user.positions.append(position)
        db.session.commit()
    return jsonify(success=not errors, errors=errors)


# Withdraw from a position, also for officer elections
@mod.route('/withdraw/', methods=['POST'])
@api_login_required
@api_requires_keys('position_id')
def withdraw():
    errors = []
    json = request.get_json(force=True)
    config = Config.query.first()

    for position in current_user.positions:
        if position.id == json['position_id']:
            position = position
            break

    if not position:
        errors.append("Not found.")
    elif config.voting_started:
        errors.append("You cannot withdraw after voting has started.")
    else:
        current_user.positions.remove(position)
        db.session.commit()

    return jsonify(success=not errors, errors=errors)
