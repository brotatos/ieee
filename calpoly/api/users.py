import calpoly.emails
from flask import Blueprint, request, flash, jsonify
from flask.ext.login import login_user, logout_user
from werkzeug import check_password_hash, generate_password_hash
from recaptcha.client.captcha import submit
from calpoly.core import db
from calpoly.users.models import User, Role
from calpoly.api.utils import delete_item, get_item, get_items, request_opts
from calpoly.api.decorators import api_login_required, api_requires_roles, \
    api_requires_keys
from calpoly.config import RECAPTCHA_PRIVATE_KEY, DEBUG, OFFICER_KEY

mod = Blueprint('users', __name__, url_prefix='/users')


@mod.route('/', methods=['GET'])
@api_login_required
def get_all():
    return jsonify(get_items('users', User, request_opts(request)))


@mod.route('/<int:id>/', methods=['DELETE'])
@api_login_required
@api_requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, User))


@mod.route('/<int:id>/', methods=['GET'])
def get(id):
    return jsonify(get_item(id, 'profile', User))


@mod.route('/login/', methods=['POST'])
@api_requires_keys('email', 'password', 'remember_me')
def login():
    errors = []
    json = request.get_json(force=True)
    user = User.query.filter_by(email=json['email']).first()

    if user is None:
        errors.append('Invalid username/password combination')

    if not errors and not check_password_hash(user.password, json['password']):
        errors.append('Invalid username/password combination')
    elif not errors:
        login_user(user, remember=json['remember_me'])

    return jsonify(success=not errors, errors=errors)


@mod.route('/logout/', methods=['GET', 'POST'])
@api_login_required
def logout():
    logout_user()
    return jsonify(success=True)


@mod.route('/', methods=['POST'])
@api_requires_keys('email', 'password', 'confirm', 'challenge', 'response')
def register():
    json = request.get_json(force=True)
    valid_officer = False
    errors = []
    user_id = None

    if not DEBUG:
        captcha_result = submit(json['challenge'], json['response'],
                                RECAPTCHA_PRIVATE_KEY, request.remote_addr)
        if not captcha_result.is_valid:
            errors.append('captcha: Validation failed.')

    if not errors:
        if '@calpoly.edu' not in json['email']:
            errors.append('You must register with a calpoly email.')

        if User.query.filter_by(email=json['email']).first():
            errors.append('An account already exists with this email.')

        if json['key'] and json['key'] == OFFICER_KEY:
            valid_officer = True
        elif json['key']:
            errors.append('Invalid officer key.')

        if len(json['password']) < 6:
            errors.append('Password must be at least 6 characters long')

        if json['password'] != json['confirm']:
            errors.append('Passwords do not match')

    if not errors:
        user = User(
            email=json['email'],
            password=generate_password_hash(json['password'])
        )

        if valid_officer:
            user.roles.append(Role.query.filter_by(name='officer').first())
            user.roles.append(Role.query.filter_by(name='voter').first())
            user.roles.append(Role.query.filter_by(name='poster').first())
        else:
            user.roles.append(Role.query.filter_by(name='unverified').first())

        db.session.add(user)
        db.session.commit()
        user_id = user.id

        login_user(user)
        flash('You are now registered!', 'success')
    return jsonify(success=not errors, errors=errors, id=user_id)


@mod.route('/reset_account/', methods=['POST'])
@api_requires_keys('email')
def reset_account():
    errors = []
    json = request.get_json(force=True)
    user = User.query.filter_by(email=json['email']).first()

    if user is None:
        errors.append('No user found with specified email address.')
    else:
        user.set_reset_password_token()
        db.session.add(user)
        db.session.commit()
        calpoly.emails.send_reset_password_email(
            user.name,
            user.email,
            request.url_root[:-4] +
            'users/reset_account/' +
            user.password_reset_token
        )
    return jsonify(success=not errors, errors=errors)


# should fix the name of this and above as well
@mod.route('/reset/', methods=['POST'])
@api_requires_keys('token', 'email', 'password')
def reset():
    """
    Resets a user's password based off a hash they were given in an e-mail.
    This route will nullify the previously set password reset token for a user.
    """
    json = request.get_json(force=True)
    errors = []
    user = User.query.filter_by(password_reset_token=json['token']).first()

    if user is None:
        errors.append('Invalid or expired token.')

    if json['confirm'] != json['password']:
        errors.append('Passwords do not match.')

    if not errors:
        if json['email']:
            user.email = json['email']
        if json['password']:
            user.password = generate_password_hash(json['password'])
        # Invalidate the users reset token
        user.password_reset_token = None
        db.session.commit()

    return jsonify(success=not errors, errors=errors)


@mod.route('/officers/', methods=['GET'])
def get_officers():
    return jsonify(
        success=True,
        officers=[i.serialize for i in User.query.
                  filter(User.roles.any(name='officer')).all()]
    )
