# -*- coding: utf-8 -*-
"""
    calpoly.api
    ~~~~~~~~~~~~

    CalPoly IEEE-SB's official API.
"""

from flask import jsonify
from calpoly import factory


def create_app(settings_override=None):
    """Returns the IEEE API application instance."""

    app = factory.create_app(__name__, __path__, settings_override)

    @app.errorhandler(404)
    def page_not_found(e):
        return jsonify(errors=["Page not found."]), 404

    @app.errorhandler(403)
    def forbidden(e):
        return jsonify(errors=["Not authorized."]), 403

    @app.errorhandler(500)
    def internal_error(e):
        return jsonify(errors=["Something has gone horribly wrong."]), 500

    return app
