from flask import Blueprint, request, jsonify
from werkzeug import check_password_hash
from calpoly.api.decorators import api_requires_keys
from calpoly.core import db
from calpoly.core_models.models import Config

mod = Blueprint('api_misc', __name__, url_prefix='')


@mod.route('/lounge_status/', methods=['POST'])
@api_requires_keys('password')
def lounge_status():
    config = Config.query.first()
    json = request.get_json(force=True)
    if check_password_hash(config.lounge_password, json['password']):
        config.lounge_open = not config.lounge_open
        db.session.commit()
        return jsonify(success=True)
    else:
        return jsonify(success=False, errors=["Invalid password."])
