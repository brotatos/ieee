Hi {{ user }},
Please go to the following link to reset your account info:
{{ url }}
Do not reply to this message.
{% include 'emails/signature.txt' %}
