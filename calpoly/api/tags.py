from flask import Blueprint, request, jsonify
from calpoly.api.utils import delete_item, get_items, request_opts
from calpoly.api.decorators import api_login_required, api_requires_roles
from calpoly.posts.models import Tag

mod = Blueprint('tags', __name__, url_prefix='/tags')


@mod.route('/', methods=['GET'])
def get_all():
    return jsonify(get_items('tags', Tag, request_opts(request)))


@mod.route('/<int:id>/', methods=['DELETE'])
@api_login_required
@api_requires_roles('webmaster')
def delete(id):
    return jsonify(delete_item(id, Tag))
