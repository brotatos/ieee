from flask import Blueprint, request, jsonify
from flask.ext.login import current_user
from calpoly.voting.utils import verify_ballot, get_results
from calpoly.core import db
from calpoly.users.models import User
from calpoly.voting.models import Position, Vote, Answer
from calpoly.api.decorators import api_login_required, api_requires_roles, \
    api_requires_keys

mod = Blueprint('voting', __name__, url_prefix='/voting')


@mod.route('/', methods=['DELETE'])
@api_login_required
@api_requires_roles('webmaster')
def clear_voting():
    Vote.query.delete()
    Answer.query.delete()
    db.engine.execute("DELETE FROM candidate")
    db.engine.execute("UPDATE users_user SET voted = false")
    db.session.commit()
    return jsonify(success=True)


@mod.route('/results/', methods=['GET'])
@api_login_required
@api_requires_roles('webmaster')
def results():
    results, write_ins, totals = get_results()

    return jsonify(success=True, results=results, write_ins=write_ins)


# takes in a JSON ballot and checks it for errors
@mod.route('/verify/', methods=['POST'])
@api_login_required
@api_requires_roles('voter')
@api_requires_keys('ballot')
def run_verify_ballot():
    return jsonify(verify_ballot(request.get_json(force=True)['ballot']))


# Submitted ballots will look like this
# ballot = {
#     "president": ["candidate"],
#     "position where two people can run": ["candidate 1", "candidate 2"]
# }
@mod.route('/submit/', methods=['POST'])
@api_login_required
@api_requires_roles('voter')
@api_requires_keys('ballot')
def submit_ballot():
    json = request.get_json(force=True)
    verify = verify_ballot(json['ballot'])

    if not verify['success']:
        return jsonify(success=False, errors=['Ballot failed verification.'])

    if current_user.voted:
        return jsonify(success=False, errors=['Forbidden.'])

    for position, choices in json['ballot'].iteritems():
        db_position = Position.query.filter_by(name=position).first()
        for choice in choices:
            if '[WRITE IN]' in choice:
                can_id = User.query.filter_by(name='WRITE_IN').first().id
                write_in = choice
            else:
                can_id = User.query.filter_by(name=choice).first().id
                write_in = ''

            vote = Vote(voter_id=current_user.id,
                        position_id=db_position.id,
                        candidate_id=can_id,
                        write_in=write_in)
            db.session.add(vote)
            db.session.commit()

    current_user.voted = True
    db.session.add(current_user)
    db.session.commit()

    return jsonify(success=True)
