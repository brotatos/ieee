import datetime
from werkzeug import generate_password_hash
from calpoly.core import db
from calpoly.models import register_to_admin
from calpoly.api.utils import unix_time, get_or_create


@register_to_admin
class ImageLink(db.Model):

    __tablename__ = 'imagelink'
    id = db.Column(db.Integer, primary_key=True)
    link = db.Column(db.String(200), nullable=False)

    def __init__(self, link=None):
        self.link = link

    def __repr__(self):
        return '<ImageLink %r>' % (self.link)

    def __unicode__(self):
        return self.link

    @staticmethod
    def sortable():
        return ['id', 'link']

    @property
    def serialize(self):
        return self.link


@register_to_admin
class Config(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    voting_started = db.Column(db.Boolean())
    lounge_open = db.Column(db.Boolean())
    run_console_visible = db.Column(db.Boolean())
    lounge_password = db.Column(db.String(120))
    officers_password = db.Column(db.String(120))
    voting_end_date = db.Column(db.DateTime())
    landing_image_id = db.Column(db.Integer, db.ForeignKey(ImageLink.id))
    officers_image_id = db.Column(db.Integer, db.ForeignKey(ImageLink.id))

    def __init__(self):
        pass

    @property
    def serialize(self):
        return {
            'voting_started': self.voting_started,
            'lounge_open': self.lounge_open,
            'run_console_visible': self.run_console_visible,
            'lounge_password': '',
            'officers_password': '',
            'voting_end_date': unix_time(self.voting_end_date),
            'landing_image': ImageLink.query.get(
                self.landing_image_id).serialize,
            'officers_image': ImageLink.query.get(
                self.officers_image_id).serialize
        }

    def edit(self, json):
        landing = get_or_create(
            json['landing_image'], ImageLink, link=json['landing_image']
        )
        officers = get_or_create(
            json['officers_image'], ImageLink, link=json['officers_image']
        )

        db.session.commit()
        self.voting_started = json['voting_started']
        self.lounge_open = json['lounge_open']
        self.run_console_visible = json['run_console_visible']
        if json['lounge_password']:
            self.lounge_password = \
                generate_password_hash(json['lounge_password'])
        if json['officers_password']:
            self.officers_password = \
                generate_password_hash(json['officers_password'])
        self.voting_end_date = datetime.datetime\
            .fromtimestamp(json['voting_end_date'])
        self.landing_image_id = landing.id
        self.officers_image_id = officers.id
        db.session.commit()
