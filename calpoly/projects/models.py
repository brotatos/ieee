import datetime
from calpoly.core import db
from calpoly.models import register_to_admin


@register_to_admin
class Project(db.Model):

    __tablename__ = 'project'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120), nullable=False, unique=True)
    url = db.Column(db.String(120), nullable=False, unique=True)
    repo = db.Column(db.String(2083))
    description = db.Column(db.Text, nullable=False)
    content = db.Column(db.Text, nullable=False)
    date = db.Column(db.DateTime)
    image_link = db.Column(db.String(2083))
    image_caption = db.Column(db.Text)
    completed = db.Column(db.Boolean())
    project_contact_id = db.Column(db.Integer, db.ForeignKey('users_user.id'))
    contact = db.relationship('User')
    associated_tag_id = db.Column(db.Integer, db.ForeignKey('tag.id'))
    associated_tag = db.relationship('Tag')

    def __init__(self, title=None, completed=False, repo=None, contact=None,
                 image=None, caption=None, description=None, content=None,
                 url=None, tag=None):
        self.title = title
        self.completed = completed
        self.repo = repo
        self.contact = contact
        self.image_link = image
        self.image_caption = caption
        self.description = description
        self.content = content
        self.url = url
        self.associated_tag = tag
        self.date = datetime.datetime.now()

    def __repr__(self):
        return '<Project %s>' % (self.title)

    @staticmethod
    def sortable():
        return ['id', 'title', 'url', 'repo', 'date']

    @property
    def serialize(self):
        if self.associated_tag:
            tag = self.associated_tag.serialize
        else:
            tag = ''

        return {
            'id': self.id,
            'title': self.title,
            'completed': self.completed,
            'repo': self.repo,
            'contact_name': self.contact.name,
            'contact_id': self.contact.id,
            'image': self.image_link,
            'caption': self.image_caption,
            'description': self.description,
            'content': self.content,
            'url': self.url,
            'associated_tag': tag
        }

    def __unicode__(self):
        return self.title
