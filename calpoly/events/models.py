from calpoly.core import db
from calpoly.models import register_to_admin


@register_to_admin
class Event(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    content = db.Column(db.String(400), unique=False)

    def __init__(self, content=None):
        self.content = content

    def __repr__(self):
        return '<Event %s>' % self.content

    @property
    def serialize(self):
        return {
            'id': self.id,
            'content': self.content
        }

    @staticmethod
    def sortable():
        return ['content', 'id']
