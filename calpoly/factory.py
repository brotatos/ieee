# -*- coding: utf-8 -*-
"""
    calpoly.factory
    ~~~~~~~~~~~~~~~

    Contains create_app method for instantiating applications.
"""

from flask import Flask
from calpoly.core import db, mail, login_manager, gravatar, compress
from calpoly import config
from calpoly.helpers import register_blueprints
import wtforms_json


def create_app(package_name, package_path, settings_override=None):
    """Returns a :class:`Flask` application instance configured with common
    functionality for the IEEE platform.

    :param package_name: application package name
    :param package_path: application package path
    :param settings_override: a dictionary of setings to override
    """
    app = Flask(package_name, instance_relative_config=True)
    app.config.from_object(config)

    # Initialize extensions.
    db.init_app(app)
    mail.init_app(app)
    login_manager.init_app(app)
    gravatar.init_app(app)
    compress.init_app(app)
    # I may need to put this in core.py later on.
    wtforms_json.init()

    register_blueprints(app, package_name, package_path)

    return app
